package com.viamedia.victor.base.api;

import android.content.Intent;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.viamedia.victor.base.MainActivity;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.models.User;

import java.io.File;
import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by marais on 2016/04/29.
 */
public class ResponseInterceptor implements Interceptor {
    private static final String TAG = "ResponseInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        // used to globally intercept responses
        Request request = chain.request();
        Response response = chain.proceed(request);

        if (response != null && response.body() != null) {
            // body().string() can only be called once, need to build new response object after use
            String bodyString = response.body().string();
            try {
                ResponseBase responseBodyObject = (new Gson()).fromJson(bodyString, ResponseBase.class);
                if (containsLast(String.valueOf(request.url()), Constants.REQUEST_URL_IGNORE_SIGN_OUT_FACEBOOK_LOGIN) ||
                        containsLast(String.valueOf(request.url()), Constants.REQUEST_URL_IGNORE_SIGN_OUT_CUSTOM_LOGIN) ||
                        containsLast(String.valueOf(request.url()), Constants.REQUEST_URL_IGNORE_SIGN_OUT_CUSTOM_REGISTRATION) ||
                        (String.valueOf(request.url()).contains(Constants.REQUEST_URL_IGNORE_SIGN_OUT_PASSWORD_RESET_PART1) && containsLast(String.valueOf(request.url()),
                                Constants.REQUEST_URL_IGNORE_SIGN_OUT_PASSWORD_RESET_PART2))) {

                    // ignore for these request urls, dont sign out
                    Log.i(TAG, "Ignore auto signout");
                } else {
                    // sign out if user session expired
                    if (responseBodyObject.getMessage().equalsIgnoreCase(Constants.RESPONSE_ERROR_MESSAGE_NO_USER_FOUND) ||
                            responseBodyObject.getMessage().equalsIgnoreCase(Constants.RESPONSE_ERROR_MESSAGE_USER_NOT_FOUND)) {
                        Log.e(TAG, responseBodyObject.getMessage());
                        signOut();
                    }
                }
            } catch (JsonSyntaxException e) {
                Log.e(TAG, e.getMessage());
            }

            // rebuild response object after body has been used
            return response
                    .newBuilder()
                    .body(ResponseBody.create(
                            response.body().contentType(),
                            bodyString
                    )).build();
        } else {
            return response;
        }
    }

    private void signOut() {
        // clear user data but persist msisdn
        User userObjectOld = MyApp.getInstance().getInstance().getStorageManager().getUserObject();
        String msisdn = userObjectOld.getMsisdnWithPlus();

        User userObjectNew = new User();
        userObjectNew.setMsisdnWithoutPlus(msisdn);

        MyApp.getInstance().getStorageManager().saveDataToFile(
                new File(MyApp.getInstance().getStorageManager().getUserFilename()),
                (new Gson()).toJson(userObjectNew, User.class)
        );

        // clear user interests data
        new File(MyApp.getInstance().getStorageManager().getInterestsFilename()).delete();

        // log out facebook user
        LoginManager.getInstance().logOut();

        // clear glide image cache
        new Thread(new Runnable() {
            public void run() {
                Glide.get(MyApp.getInstance()).clearDiskCache();
                Glide.get(MyApp.getInstance()).clearMemory();
            }
        });

        // reload main with logged out user
        Intent i = new Intent(new Intent(MyApp.getInstance(), MainActivity.class));
        // set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        MyApp.getInstance().startActivity(i);
    }

    private boolean containsLast(String s, String sLast) {
        int index = s.indexOf(sLast);
        return index + sLast.length() == s.length();
    }
}