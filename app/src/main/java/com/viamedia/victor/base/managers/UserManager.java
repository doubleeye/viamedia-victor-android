package com.viamedia.victor.base.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.viamedia.victor.base.constants.Constants;

public class UserManager {
    private static final String TAG = "UserManager";
    private String country;

    private SharedPreferences preferences;
    private Context context;

    public UserManager(Context context) {
        this.context = context;

        if (preferences == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    public String getCountry() {
        country = preferences.getString(Constants.SHARED_PREF_COUNTRYCODE, null);
        return country;
    }

    public int getCountryDialCode() {
        return PhoneNumberUtil.getInstance().getCountryCodeForRegion(getCountry());
    }

    public void setCountry(String country) {
        preferences.edit().putString(Constants.SHARED_PREF_COUNTRYCODE, country).apply();
        this.country = country;
    }
}
