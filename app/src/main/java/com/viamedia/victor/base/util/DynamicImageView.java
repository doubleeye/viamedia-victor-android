package com.viamedia.victor.base.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class DynamicImageView extends ImageView {
    private static final String TAG = "DynamicImageView";

    public DynamicImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final Drawable drawable = this.getDrawable();

        if (drawable != null) {
            if (drawable.getIntrinsicWidth() < MeasureSpec.getSize(widthMeasureSpec)) {
                Log.i(TAG, "whitespace left and right " + getContext());
                this.setMeasuredDimension(
                        MeasureSpec.getSize(heightMeasureSpec),
                        MeasureSpec.getSize(widthMeasureSpec)
                );
            } else if (drawable.getIntrinsicHeight() < MeasureSpec.getSize(heightMeasureSpec)) {
                Log.i(TAG, "whitespace top and bottom " + getContext());
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            } else {
                Log.i(TAG, "no whitespace " + drawable.getIntrinsicWidth() + "x" + drawable.getIntrinsicHeight() + " - " + MeasureSpec.getSize(widthMeasureSpec) + "x" + MeasureSpec.getSize(heightMeasureSpec) + " - " + getContext());
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            }
        } else {
            Log.i(TAG, "no drawable " + getContext());
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
