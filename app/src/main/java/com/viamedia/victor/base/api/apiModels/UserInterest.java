package com.viamedia.victor.base.api.apiModels;

import java.util.Locale;

public class UserInterest {
    private int id;
    private String name;
    private String slug;

    public UserInterest(int id, String name) {
        this.id = id;
        this.setName(name);
        this.setSlug(name.toLowerCase(Locale.getDefault()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
