package com.viamedia.victor.base.api.apiModels;

import com.viamedia.victor.base.models.Comment;

import java.util.ArrayList;

public class ResponseBaseArticleComments extends ResponseBase {
    private ArrayList<Comment> data;

    public ArrayList<Comment> getData() {
        return data;
    }

    public void setData(ArrayList<Comment> data) {
        this.data = data;
    }
}