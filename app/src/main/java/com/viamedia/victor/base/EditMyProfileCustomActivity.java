package com.viamedia.victor.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestUpdateUser;
import com.viamedia.victor.base.api.apiModels.ResponseBaseLoginRegistration;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.PermissionManager;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.ValidationManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.DynamicImageView;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

import java.io.File;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMyProfileCustomActivity extends AppCompatActivity {

    private static final String TAG = "EditMyProfeCustActivity";
    private Context context;
    private Activity activity;
    private StorageManager storageManager;
    private ValidationManager validationManager;
    private PermissionManager permissionManager;
    private WebService webService;
    private Util util;
    private ImageUtil imageUtil;
    private EditText firstname, surname, age, email, city, country;
    private Button doneButton;
    private DynamicImageView profileImage;
    private User user;
    private RadioButton radioButtonMale, radioButtonFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_profile_custom);
        activity = this;
        context = this;

        initManagers();
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        imageUtil = MyApp.getInstance().getImageUtil();
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
        validationManager = MyApp.getInstance().getValidationManager();
        permissionManager = MyApp.getInstance().getPermissionManager();
    }

    private void initUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioButtonMale = (RadioButton) findViewById(R.id.radio_button_male);
        radioButtonFemale = (RadioButton) findViewById(R.id.radio_button_female);

        firstname = (EditText) findViewById(R.id.first_name);
        surname = (EditText) findViewById(R.id.surname);
        age = (EditText) findViewById(R.id.age);
        email = (EditText) findViewById(R.id.email);
        city = (EditText) findViewById(R.id.city);
        country = (EditText) findViewById(R.id.country);
        doneButton = (Button) findViewById(R.id.positive_button);
        profileImage = (DynamicImageView) findViewById(R.id.image_view);

        // ui actions
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (user != null) {
                    AppCompatRadioButton radioButton = (AppCompatRadioButton) group.findViewById(checkedId);
                    if (radioButton != null) {
                        user.setGender(String.valueOf(radioButton.getText()).toLowerCase(Locale.getDefault()));
                    }
                }
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValidation();
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If the user accepts one permission in a group/category they accept the entire group
                if (permissionManager.hasPermission(context, Constants.PERMISSION_READ_EXTERNAL_STORAGE)) {
                    // continue with normal operation
                    cameraChooseDialogCameraGallery();
                } else {
                    if (permissionManager.shouldWeAsk(context, Constants.PERMISSION_READ_EXTERNAL_STORAGE)) {
                        String[] perms = {Constants.PERMISSION_READ_EXTERNAL_STORAGE};
                        permissionManager.askPermissions(activity, 200, perms);
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_denied_by_user, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                boolean perm1 = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (perm1) {
                    // If the user accepts one permission in a group/category they accept the entire group
                    Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_successfully_granted, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void cameraChooseDialogCameraGallery() {
        AlertDialog.Builder myAlertDialog = imageUtil.showChooserDialogCameraGallery(this);
        myAlertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        imageUtil.onActivityResultCameraGallery(
                resultCode,
                requestCode,
                intent,
                this,
                profileImage,
                user);
    }

    public void submitValidation() {
        if (validationManager.isEditMyProfileCustomFormValid(
                findViewById(android.R.id.content),
                firstname,
                surname,
                age,
                email,
                city,
                country)) {

            apiUpdateUser(
                    util.removePlusFromNumber(user.getMsisdnWithoutPlus()),
                    String.valueOf(firstname.getText()),
                    String.valueOf(surname.getText()),
                    String.valueOf(age.getText()),
                    user.getGender().equalsIgnoreCase(Constants.MALE) ? Constants.M : Constants.F,
                    user.getGender(),
                    String.valueOf(email.getText()),
                    String.valueOf(city.getText()),
                    String.valueOf(country.getText()),
                    user.getLoginToken());
        }
    }

    private void apiUpdateUser(
            final String msisdn,
            final String firstName,
            final String lastName,
            final String age,
            final String genderShort,
            final String gender,
            final String email,
            final String city,
            final String country,
            final String token) {

        showLoading();

        final RequestUpdateUser requestUpdateUser = new RequestUpdateUser();
        requestUpdateUser.setToken(token);
        requestUpdateUser.setFirstName(firstName);
        requestUpdateUser.setLastName(lastName);
        requestUpdateUser.setEmail(email);
        requestUpdateUser.setAge(age);
        requestUpdateUser.setGender(genderShort);
        requestUpdateUser.setAvatar(user.getAvatarBase64String());
        requestUpdateUser.setCity(city);
        requestUpdateUser.setCountry(country);

        Call<ResponseBaseLoginRegistration> call = webService.update(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                msisdn,
                requestUpdateUser
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();
                Snackbar.make(findViewById(android.R.id.content), R.string.success, Snackbar.LENGTH_LONG).show();

                user.setFirstname(firstName);
                user.setLastname(lastName);
                user.setAge(age);
                user.setGender(gender);
                user.setEmail(email);
                user.setCity(city);
                user.setCountry(country);

                // only save data if call succeeds
                storageManager.saveDataToFile(
                        new File(storageManager.getUserFilename()),
                        (new Gson()).toJson(user)
                );

                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                    }
                });
                t.start();
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.update_user_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void loadData() {
        // load in user object data if not set already
        if (user == null) {
            user = storageManager.getUserObject();
        }

        if (storageManager != null) {
            if (firstname.getText().length() == 0 && user.getFirstname() != null) {
                firstname.setText(storageManager.getUserObject().getFirstname());
            }
            if (surname.getText().length() == 0 && user.getLastname() != null) {
                surname.setText(storageManager.getUserObject().getLastname());
            }
            if (age.getText().length() == 0 && user.getAge() != null) {
                age.setText(storageManager.getUserObject().getAge());
            }
            if (!radioButtonMale.isChecked() && !radioButtonFemale.isChecked() && user.getGender() != null) {
                if (user.getGender().equalsIgnoreCase(Constants.MALE)) {
                    radioButtonMale.setChecked(true);
                } else if (user.getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    radioButtonFemale.setChecked(true);
                } else {
                    radioButtonMale.setChecked(true);
                }
            }
            if (email.getText().length() == 0 && user.getEmail() != null) {
                email.setText(storageManager.getUserObject().getEmail());
            }
            if (city.getText().length() == 0 && user.getCity() != null) {
                city.setText(storageManager.getUserObject().getCity());
            }
            if (country.getText().length() == 0 && user.getCountry() != null) {
                country.setText(storageManager.getUserObject().getCountry());
            }
            if (user.getAvatarBitmap(imageUtil) != null) {
                // load profile image from device
                profileImage.setImageBitmap(user.getAvatarBitmap(imageUtil));
            } else if (user.getAvatar() != null) {
                // load profile image from url
                Glide
                        .with(context)
                        .load(user.getAvatar())
                        .crossFade()
                        .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new image once a day
                        .error(R.drawable.flavoured_category_banner) // fallback image
                        .into(profileImage);
            }
        }
    }
}