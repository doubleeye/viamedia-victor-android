package com.viamedia.victor.base.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.models.ArticleLocal;

import java.util.ArrayList;

public class VoucherListAdapter extends RecyclerView.Adapter<VoucherListAdapter.ViewHolder> {
    private static final String TAG = "VoucherListAdapter";
    private ArrayList<ArticleLocal> data;
    private AdapterInterface adapterInterface;
    private Context context;

    public VoucherListAdapter(Context context, ArrayList<ArticleLocal> data, AdapterInterface adapterInterface) {
        this.data = data;
        this.context = context;
        this.adapterInterface = adapterInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView title;
        public ImageView image;
        public ProgressBar imageProgress;

        public ViewHolder(final View itemView, final AdapterInterface adapterInterface) {
            super(itemView);
            mView = itemView;

            // ui
            title = (TextView) itemView.findViewById(R.id.item_title);
            image = (ImageView) itemView.findViewById(R.id.category_adapter_item_image);
            imageProgress = (ProgressBar) itemView.findViewById(R.id.image_progress);

            // click activate or deactivate item
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterInterface.interfaceResponse(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public VoucherListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardview = LayoutInflater.from(parent.getContext()).inflate(R.layout.voucher_list_item_layout, parent, false);
        return new ViewHolder(cardview, adapterInterface);
    }

    @Override
    public void onBindViewHolder(final VoucherListAdapter.ViewHolder holder, int position) {
        // default view
        holder.title.setText(data.get(position).getTitle());

        // load image from url
        Glide.clear(holder.image);
        Glide
                .with(context)
                .load(data.get(position).getImageUrl())
                .crossFade()
                .error(R.drawable.flavoured_category_banner) // fallback image
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        // hide image progress
                        if (holder.imageProgress != null) {
                            holder.imageProgress.setVisibility(View.GONE);
                        }

                        if (e != null) {
                            Log.e(TAG, e.getMessage());
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        // hide image progress
                        if (holder.imageProgress != null) {
                            holder.imageProgress.setVisibility(View.GONE);
                        }
                        return false;
                    }
                })
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface AdapterInterface {
        void interfaceResponse(int position);
    }
}
