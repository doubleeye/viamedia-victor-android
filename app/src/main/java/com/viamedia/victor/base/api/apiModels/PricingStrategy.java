package com.viamedia.victor.base.api.apiModels;

public class PricingStrategy {
    private int id;
    private double price;
    private String status;
    private String type;
    private boolean credits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCredits(boolean credits) {
        this.credits = credits;
    }

    public boolean isCredits() {
        return credits;
    }
}
