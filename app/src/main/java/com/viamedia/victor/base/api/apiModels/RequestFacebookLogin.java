package com.viamedia.victor.base.api.apiModels;

public class RequestFacebookLogin {
    private String facebook_user_id;
    private String facebook_token;
    private String email;

    public String getFacebookUserId() {
        return facebook_user_id;
    }

    public void setFacebookUserId(String facebook_user_id) {
        this.facebook_user_id = facebook_user_id;
    }

    public String getFacebookToken() {
        return facebook_token;
    }

    public void setFacebookToken(String facebook_token) {
        this.facebook_token = facebook_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
