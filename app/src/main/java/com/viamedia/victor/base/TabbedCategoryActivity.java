package com.viamedia.victor.base;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.viamedia.victor.base.adapters.TabbedArticleAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.util.Util;

import java.util.ArrayList;

public class TabbedCategoryActivity extends BaseActivityArticleList {
    private static final String TAG = "TabbedCategoryActivity";
    private TabbedArticleAdapter tabbedArticleAdapter;
    private ViewPager tabViewPager;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private UserManager userManager;
    private Util util;
    private WebService webService;
    private StorageManager storageManager;
    private ArrayList<ResponseBaseStoreFront.DataObject.ResponseStoreFrontSection.ResponseStoreFrontSectionCategory> categories;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_category);

        initManagers();
        initUi();
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUi() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Set up the ViewPager with the sections adapter.
        tabViewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        userManager = MyApp.getInstance().getUserManager();
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void loadData() {
        if (getIntent().hasExtra(Constants.EXTRA_TABBED_CATEGORY_TITLE) &&
                getIntent().hasExtra(Constants.EXTRA_TABBED_CATEGORY_SECTION_ID)) {

            setTitle(getIntent().getStringExtra(Constants.EXTRA_TABBED_CATEGORY_TITLE));

            if (storageManager.getStoreFrontObject() != null &&
                    storageManager.getStoreFrontObject().getData() != null &&
                    storageManager.getStoreFrontObject().getData().getSectionsWithoutHome() != null) {

                categories = storageManager.
                        getStoreFrontObject().
                        getData().
                        getSectionsWithoutHome().
                        get(getIntent().getIntExtra(Constants.EXTRA_TABBED_CATEGORY_SECTION_ID, 0)).
                        getCategories();

                // Create the adapter that will return a fragment
                // primary sections of the activity.
                tabbedArticleAdapter = new TabbedArticleAdapter(
                        categories,
                        getSupportFragmentManager(),
                        util);

                tabViewPager.setAdapter(tabbedArticleAdapter);
                tabLayout.setupWithViewPager(tabViewPager);
                tabLayout.setTabTextColors(
                        ContextCompat.getColor(this, R.color.white),
                        ContextCompat.getColor(this, R.color.white)
                );
            }
        } else {
            finish();
        }
    }

    public WebService getWebService() {
        return webService;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public Util getUtil() {
        return util;
    }
}