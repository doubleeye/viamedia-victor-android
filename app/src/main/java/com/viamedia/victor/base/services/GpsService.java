package com.viamedia.victor.base.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.api.apiModels.GpsServiceObject;
import com.viamedia.victor.base.managers.StorageManager;

import java.io.File;

public class GpsService extends Service {
    private static final String TAG = "GpsService";
    private LocationListener locationListener;
    private LocationManager locationManager;
    private StorageManager storageManager;
    private GpsServiceObject gpsServiceObject;
    private boolean canStopService = false;
    private Handler timeoutHandler;
    private Runnable timeoutRunnable;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand()");
        gpsServiceObject = null;
        canStopService = false;
        initManagers();
        setupGpsListner();
        startGpsListner();
        return START_STICKY; // Let it continue running until it is stopped.
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();

        saveData();
        stopGpsListner();
        timeoutHandler.removeCallbacks(timeoutRunnable);

        if (!canStopService) {
            Log.i(TAG, "onDestroy() keeping service alive");
            startService(new Intent(this, GpsService.class));
        }
    }

    private void saveData() {
        if (gpsServiceObject != null) {
            storageManager.saveDataToFile(
                    new File(storageManager.getGpsServiceObjectFilename()),
                    (new Gson()).toJson(gpsServiceObject, GpsServiceObject.class)
            );
        }
    }

    private void initManagers() {
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }

        if (storageManager == null) {
            storageManager = MyApp.getInstance().getStorageManager();
        }
    }

    private void stopGpsListner() {
        Log.i(TAG, "stopGpsListner()");
        if (locationListener != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.removeUpdates(locationListener);
            }
        }
    }

    private void startGpsListner() {
        Log.i(TAG, "startGpsListner()");
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        this.getResources().getInteger(R.integer.gps_service_min_time_ms),
                        this.getResources().getInteger(R.integer.gps_service_min_distance_meters),
                        locationListener
                );
            }
        }

        // gps timeout
        timeoutHandler = new Handler();
        timeoutHandler.postDelayed(timeoutRunnable = new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "gpsTimeout()");
                canStopService = true;
                onDestroy();
            }
        }, this.getResources().getInteger(R.integer.gps_service_timeout_ms));
    }

    private void setupGpsListner() {
        if (locationListener == null) {
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    Log.i(TAG, "onLocationChanged()");
                    gpsServiceObject = new GpsServiceObject(
                            location.getLatitude(),
                            location.getLongitude()
                    );
                    canStopService = true;
                    onDestroy();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                }

                @Override
                public void onProviderDisabled(String s) {
                }
            };
        }
    }
}