package com.viamedia.victor.base.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.TabbedCategoryActivity;
import com.viamedia.victor.base.adapters.ArticleListAdapter;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.constants.Constants;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleListFragmentType1 extends Fragment {
    private static final String TAG = "FragmentType1";
    private static final String ARG_PAGE_TITLE = "pageTitle";
    private static final String ARG_PAGE_CONTENT_SELECTION = "pageContentSelection";
    private TabbedCategoryActivity activity;
    private RecyclerView recyclerView;
    private TextView recyclerEmptyText;
    private ArticleListAdapter adapter;
    private View rootView;

    public static ArticleListFragmentType1 newInstance(String pageTitle, String contentSelection) {
        ArticleListFragmentType1 fragment = new ArticleListFragmentType1();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE_TITLE, pageTitle);
        args.putString(ARG_PAGE_CONTENT_SELECTION, contentSelection);

        fragment.setArguments(args);
        return fragment;
    }

    public ArticleListFragmentType1() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        activity = (TabbedCategoryActivity) getActivity();

        initUi(rootView);
        apiContentSelection(rootView);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initUi(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerEmptyText = (TextView) rootView.findViewById(R.id.recycler_view_empty_text_view);
    }

    private void apiContentSelection(final View rootView) {
        showLoadingFragment(rootView);

        Call<ResponseBaseContentSelectionOrSection> call = activity.getWebService().getContentSelection(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                String.format(getString(R.string.items_range), 0, getResources().getInteger(R.integer.max_result_content_selection)),
                activity.getStorageManager() != null && activity.getStorageManager().getUserObject() != null ? activity.getStorageManager().getUserObject().getLoginToken() : null,
                activity.getStorageManager() != null && activity.getStorageManager().getGpsServiceObject() != null ? activity.getStorageManager().getGpsServiceObject().getLatString() : null,
                activity.getStorageManager() != null && activity.getStorageManager().getGpsServiceObject() != null ? activity.getStorageManager().getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                String.valueOf(getArguments().get(ARG_PAGE_CONTENT_SELECTION))
        );
        call.enqueue(new Callback<ResponseBaseContentSelectionOrSection>() {
            @Override
            public void onResponse(Call<ResponseBaseContentSelectionOrSection> call, final Response<ResponseBaseContentSelectionOrSection> response) {
                Log.i(TAG, "onSuccess()");
                if (response.body() != null &&
                        response.body().isSuccess() &&
                        response.body().getData() != null &&
                        response.body().getData().getResults() != null &&
                        response.body().getData().getResults().getContentItems() != null) {

                    // save for offline use
                    activity.getStorageManager().saveDataToFile(
                            new File(activity.getStorageManager().getContentSelectionFilename(
                                    String.valueOf(getArguments().get(ARG_PAGE_CONTENT_SELECTION))
                            )),
                            (new Gson()).toJson(response.body())
                    );

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.body().getData().getResults().getContentItems().size() > 0) {
                                recyclerEmptyText.setVisibility(View.GONE);
                            } else {
                                recyclerEmptyText.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } else {
                    Log.e(TAG, "Failed to retrieve data");
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(activity.findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                            recyclerEmptyText.setVisibility(View.VISIBLE);
                        }
                    });
                }

                // load data
                initAdapterWithData(rootView, activity.getStorageManager().getContentSelectionObject(
                        String.valueOf(getArguments().get(ARG_PAGE_CONTENT_SELECTION))
                ));
                hideLoadingFragment(rootView);
            }

            @Override
            public void onFailure(Call<ResponseBaseContentSelectionOrSection> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(activity.findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                        recyclerEmptyText.setVisibility(View.VISIBLE);

                        // load offline data
                        initAdapterWithData(rootView, activity.getStorageManager().getContentSelectionObject(
                                String.valueOf(getArguments().get(ARG_PAGE_CONTENT_SELECTION))
                        ));
                    }
                });
                hideLoadingFragment(rootView);
            }
        });
    }

    private void initAdapterWithData(final View rootView, ResponseBaseContentSelectionOrSection result) {
        if (result != null && result.getData() != null && recyclerView != null) {
            // new adapter
            adapter = new ArticleListAdapter(activity, result, null, 2, new ArticleListAdapter.AdapterInterface() {
                @Override
                public void interfaceResponse(int position) {
                    activity.handleIntefaceResponse(
                            position,
                            activity.getStorageManager(),
                            activity.getUtil(),
                            activity.getWebService(),
                            activity,
                            adapter);
                }
            });
            recyclerView.setAdapter(adapter);
        }
    }

    public void showLoadingFragment(View rootView) {
        LinearLayout loadingLayout = (LinearLayout) rootView.findViewById(R.id.loading);
        if (loadingLayout != null) {
            loadingLayout.setVisibility(View.VISIBLE);
        }
    }

    public void hideLoadingFragment(View rootView) {
        LinearLayout loadingLayout = (LinearLayout) rootView.findViewById(R.id.loading);
        if (loadingLayout != null) {
            loadingLayout.setVisibility(View.GONE);
        }
    }
}