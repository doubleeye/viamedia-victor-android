package com.viamedia.victor.base;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestRegistration;
import com.viamedia.victor.base.api.apiModels.ResponseBaseLoginRegistration;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.managers.ValidationManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.Util;

import java.io.File;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {
    private static final String TAG = "RegistrationActivity";
    private Button submitButton;
    private EditText firstname, surname, age, txtMsisdn, password, passwordConfirmation, email, city, country;
    private String formattedNumber;
    private RadioButton radioButtonMale, radioButtonFemale;
    private UserManager userManager;
    private Util util;
    private WebService webService;
    private ValidationManager validationManager;
    private StorageManager storageManager;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initManagers();
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadUserData();
    }

    @Override
    protected void onPause() {
        saveUserData();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
        finish();

        super.onBackPressed();
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        userManager = MyApp.getInstance().getUserManager();
        webService = MyApp.getInstance().getWebService();
        validationManager = MyApp.getInstance().getValidationManager();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void initUi() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioButtonMale = (RadioButton) findViewById(R.id.radio_button_male);
        radioButtonFemale = (RadioButton) findViewById(R.id.radio_button_female);

        firstname = (EditText) findViewById(R.id.first_name);
        surname = (EditText) findViewById(R.id.surname);
        age = (EditText) findViewById(R.id.age);
        txtMsisdn = (EditText) findViewById(R.id.msisdn);
        city = (EditText) findViewById(R.id.city);
        country = (EditText) findViewById(R.id.country);
        password = (EditText) findViewById(R.id.password);
        passwordConfirmation = (EditText) findViewById(R.id.confirm_password);
        email = (EditText) findViewById(R.id.email);
        submitButton = (Button) findViewById(R.id.register_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.sign_up);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // actions
        uiMsisdnTextChangeValidation();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValidation();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (user != null) {
                    AppCompatRadioButton radioButton = (AppCompatRadioButton) group.findViewById(checkedId);
                    if (radioButton != null) {
                        user.setGender(String.valueOf(radioButton.getText()).toLowerCase(Locale.getDefault()));
                    }
                }
            }
        });
    }

    private void uiMsisdnTextChangeValidation() {
        txtMsisdn.addTextChangedListener(
                new PhoneNumberFormattingTextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // submitValidation
                        formattedNumber = validationManager.isPhoneNumberValid(String.valueOf(s));
                        if (formattedNumber != null) {
                            submitButton.setEnabled(true);
                        } else {
                            submitButton.setEnabled(false);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                }
        );
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void showMainAcitvity() {
        showLoading();

        Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
        intent.putExtra(Constants.EXTRA_REGISTRATION_SUCCESS, true);
        startActivity(intent);
        finish();
    }

    public void submitValidation() {
        if (validationManager.isRegistrationFormValid(
                findViewById(android.R.id.content),
                firstname,
                surname,
                password,
                passwordConfirmation,
                age,
                email,
                city,
                country)) {

            apiRegister(util.removePlusFromNumber(formattedNumber),
                    String.valueOf(firstname.getText()),
                    String.valueOf(surname.getText()),
                    String.valueOf(age.getText()),
                    radioButtonMale.isChecked() ? Constants.M : Constants.F,
                    radioButtonMale.isChecked() ? Constants.MALE : Constants.FEMALE,
                    String.valueOf(password.getText()),
                    String.valueOf(passwordConfirmation.getText()),
                    String.valueOf(email.getText()),
                    String.valueOf(city.getText()),
                    String.valueOf(country.getText()));
        }
    }

    private void saveUserData() {
        if (user == null) {
            // try to load from local device 1st
            user = storageManager.getUserObject();

            // fallback
            if (user == null) {
                user = new User();
            }
        }

        user.setFirstname(String.valueOf(firstname.getText()));
        user.setLastname(String.valueOf(surname.getText()));
        user.setMsisdnWithoutPlus(formattedNumber);
        user.setEmail(String.valueOf(email.getText()));
        user.setAge(String.valueOf(age.getText()));
        user.setGender(user.getGender());
        user.setCity(String.valueOf(city.getText()));
        user.setCountry(String.valueOf(country.getText()));
        storageManager.saveDataToFile(
                new File(storageManager.getUserFilename()),
                (new Gson()).toJson(user)
        );
    }

    private void loadUserData() {
        user = storageManager.getUserObject();

        // default/fallback
        txtMsisdn.setText(
                String.format(
                        Locale.getDefault(),
                        getString(R.string.msisdn_with_plus),
                        MyApp.getInstance().getUserManager().getCountryDialCode()));

        if (user != null) {
            if (user.getMsisdnWithoutPlus() != null && user.getMsisdnWithoutPlus().length() > 0) {
                formattedNumber = validationManager.isPhoneNumberValid(user.getMsisdnWithoutPlus());
                if (formattedNumber == null) {
                    formattedNumber = validationManager.isPhoneNumberValid(user.getMsisdnWithPlus());
                }

                if (formattedNumber != null) {
                    txtMsisdn.setText(formattedNumber);
                }
            }
            if (firstname.getText().length() == 0 && user.getFirstname() != null) {
                firstname.setText(user.getFirstname());
            }
            if (surname.getText().length() == 0 && user.getLastname() != null) {
                surname.setText(user.getLastname());
            }
            if (age.getText().length() == 0 && user.getAge() != null) {
                age.setText(user.getAge());
            }
            if (!radioButtonMale.isChecked() && !radioButtonFemale.isChecked() && user.getGender() != null) {
                if (user.getGender().equalsIgnoreCase(Constants.MALE)) {
                    radioButtonMale.setChecked(true);
                } else if (user.getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    radioButtonFemale.setChecked(true);
                }
            }
            if (!radioButtonMale.isChecked() && !radioButtonFemale.isChecked()) {
                radioButtonMale.setChecked(true);
            }
            if (email.getText().length() == 0 && user.getEmail() != null) {
                email.setText(user.getEmail());
            }
            if (city.getText().length() == 0 && user.getCity() != null) {
                city.setText(user.getCity());
            }
            if (country.getText().length() == 0 && user.getCountry() != null) {
                country.setText(user.getCountry());
            }
        }

        // update cursor position of first field
        firstname.setSelection(firstname.getText().length());
    }

    private void apiRegister(final String msisdn,
                             final String firstname,
                             final String surname,
                             final String age,
                             final String genderShort,
                             final String gender,
                             final String password,
                             final String passwordConfirmation,
                             final String email,
                             final String city,
                             final String country) {
        showLoading();

        final RequestRegistration requestRegistration = new RequestRegistration();
        requestRegistration.setMsisdn(msisdn);
        requestRegistration.setFirstName(firstname);
        requestRegistration.setLastName(surname);
        requestRegistration.setPassword(password);
        requestRegistration.setPasswordConfirmation(passwordConfirmation);
        if (email.length() > 0) {
            requestRegistration.setEmail(email);
        }
        requestRegistration.setAge(age);
        requestRegistration.setGender(genderShort);
        requestRegistration.setCity(city);
        requestRegistration.setCountry(country);
        // upload avatar after registration
        requestRegistration.setDeviceCode(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.GCM_CLIENT_TOKEN, null));

        Call<ResponseBaseLoginRegistration> call = webService.register(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                requestRegistration
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response.body() != null && response.body().getData() != null && response.body().getData().getToken() != null) {
                    // on success registration, token will be received, user will be logged in automatically on server side
                    // token will be used for future calls after login
                    Log.i(TAG, "login token: " + response.body().getData().getToken());

                    // save user data
                    user.setLoginToken(response.body().getData().getToken());
                    user.setId(response.body().getData().getId());
                    user.setFirstname(firstname);
                    user.setLastname(surname);
                    user.setEmail(email);

                    user.setCity(city);
                    user.setCountry(country);
                    user.setAge(age);
                    user.setGender(gender);

                    // show main activity
                    showMainAcitvity();
                } else if (response.body() != null && !response.body().isSuccess() && response.body().getResponseCode().equalsIgnoreCase(Constants.RESPONSE_ERROR_CODE_VALIDATION_FAILED)) {
                    // TODO: server needs to fix [[error responses]]
//                    String errors = response.body().getErrorsAsString();
//                    if (errors != null) {
//                        Snackbar.make(findViewById(android.R.id.content), errors, Snackbar.LENGTH_LONG).show();
//                    } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_up_failed_please_try_again, Snackbar.LENGTH_LONG).show();
//                    }
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_up_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.sign_up_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }
}