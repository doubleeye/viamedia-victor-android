package com.viamedia.victor.base.api;

import com.viamedia.victor.base.api.apiModels.RequestAddComment;
import com.viamedia.victor.base.api.apiModels.RequestFacebookLogin;
import com.viamedia.victor.base.api.apiModels.RequestLogin;
import com.viamedia.victor.base.api.apiModels.RequestLogout;
import com.viamedia.victor.base.api.apiModels.RequestPasswordReset;
import com.viamedia.victor.base.api.apiModels.RequestPayment;
import com.viamedia.victor.base.api.apiModels.RequestRegistration;
import com.viamedia.victor.base.api.apiModels.RequestSaveUserInterests;
import com.viamedia.victor.base.api.apiModels.RequestUpdateUser;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.api.apiModels.ResponseBaseArticleComments;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.api.apiModels.ResponseBaseLoginRegistration;
import com.viamedia.victor.base.api.apiModels.ResponseBaseSearch;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebService {
    @POST("/api/{apiVersion}/users/login")
    Call<ResponseBaseLoginRegistration> login(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Body RequestLogin body
    );

    @POST("/api/{apiVersion}/users/facebook-login")
    Call<ResponseBaseLoginRegistration> facebookLogin(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Body RequestFacebookLogin body
    );

    @PUT("/api/{apiVersion}/users/logout/{msisdn}")
    Call<ResponseBaseLoginRegistration> logout(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("msisdn") String msisdn,
            @Body RequestLogout body
    );

    @POST("/api/{apiVersion}/users")
    Call<ResponseBaseLoginRegistration> register(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Body RequestRegistration body
    );

    @PUT("/api/{apiVersion}/users/{msisdn}")
    Call<ResponseBaseLoginRegistration> update(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("msisdn") String msisdn,
            @Body RequestUpdateUser body
    );

    @GET("/api/{apiVersion}/cms/storefronts/{storeFront}")
    Call<ResponseBaseStoreFront> getStoreFrontList(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("storeFront") String storeFront
    );

    // contains multiple sections
    @GET("/api/{apiVersion}/cms/content-selection/{contentSelection}")
    Call<ResponseBaseContentSelectionOrSection> getContentSelection(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("range") String range,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("contentSelection") String contentSelection
    );

    // multiple sections in one content selection
    @GET("/api/{apiVersion}/cms/storefronts/sections/{section}")
    Call<ResponseBaseContentSelectionOrSection> getSection(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("section") String section
    );

    @GET("/api/{apiVersion}/cms/storefronts/landing")
    Call<ResponseBaseContentSelectionOrSection> getHomeFeed(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("range") String range,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion
    );

    @GET("/api/{apiVersion}/cms/search")
    Call<ResponseBaseSearch> getSearch(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("range") String range,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Query("q") String q
    );

    @GET("/api/{apiVersion}/cms/contents/comments/{articleId}")
    Call<ResponseBaseArticleComments> getArticleComments(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("articleId") String articleId
    );

    @POST("/api/{apiVersion}/cms/contents/{articleId}/add-comment")
    Call<ResponseBaseArticleComments> addComment(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("articleId") String articleId,
            @Body RequestAddComment body
    );

    @PUT("/api/{apiVersion}/users/interest/{msisdn}")
    Call<ResponseBase> saveUserInterests(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Header("lat") String lat,
            @Header("lng") String lng,
            @Path("apiVersion") String apiVersion,
            @Path("msisdn") String msisdn,
            @Body RequestSaveUserInterests body
    );

    @GET("/api/{apiVersion}/users/{msisdn}/edit")
    Call<ResponseBase> requestPasswordReset(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Path("apiVersion") String apiVersion,
            @Path("msisdn") String msisdn
    );

    @POST("/api/{apiVersion}/billing/payment")
    Call<ResponseBase> makePaymentBilling(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Path("apiVersion") String apiVersion,
            @Body RequestPayment body
    );

    @PUT("/api/{apiVersion}/cms/contents/{contentSelection}/credits/{articleId}")
    Call<ResponseBase> checkAndDeductCreditsForArticle(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Header("token") String token,
            @Path("apiVersion") String apiVersion,
            @Path("contentSelection") String contentSelection,
            @Path("articleId") String articleId
    );

    @PUT("/api/{apiVersion}/users/reset-password/{msisdn}")
    Call<ResponseBase> resetPassword(
            @Header("Content-type") String contentType,
            @Header("x-api-key") String apiKey,
            @Header("channel-id") String channelId,
            @Header("app-id") String appId,
            @Path("apiVersion") String apiVersion,
            @Path("msisdn") String msisdn,
            @Body RequestPasswordReset body
    );
}