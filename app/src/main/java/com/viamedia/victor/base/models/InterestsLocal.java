package com.viamedia.victor.base.models;

import java.util.ArrayList;

public class InterestsLocal {
    ArrayList<InterestLocal> interestsLocal;

    public ArrayList<InterestLocal> getInterestsLocal() {
        return interestsLocal;
    }

    public ArrayList<InterestLocal> getInterestsLocalSelected() {
        ArrayList<InterestLocal> interestsLocalSelected = new ArrayList<>();
        for (InterestLocal interestLocal : interestsLocal) {
            if (interestLocal.isEnabled()) {
                interestsLocalSelected.add(interestLocal);
            }
        }
        return interestsLocalSelected;
    }

    public void setInterestsLocal(ArrayList<InterestLocal> interestsLocal) {
        this.interestsLocal = interestsLocal;
    }
}
