package com.viamedia.victor.base.models;

import com.viamedia.victor.base.MyApp;

import java.util.ArrayList;
import java.util.Date;

public class Comment {
    private int id;
    private String content;
    private String created_at;
    private CommentAuthor author;
    private ArrayList<CommentReply> replies;

    public class CommentAuthor {
        private String name;
        private String avatar;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

    private class CommentReply {
        private int id;
        private String content;
        private String created_at;
        private CommentAuthor author;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreatedAt() {
            return created_at;
        }

        public void setCreatedAt(String created_at) {
            this.created_at = created_at;
        }

        public CommentAuthor getAuthor() {
            return author;
        }

        public void setAuthor(CommentAuthor author) {
            this.author = author;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public Date getCreatedAtAsDate() {
        return MyApp.getInstance().getUtil().parseTime(this.created_at);
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public CommentAuthor getAuthor() {
        return author;
    }

    public void setAuthor(CommentAuthor author) {
        this.author = author;
    }

    public ArrayList<CommentReply> getReplies() {
        return replies;
    }

    public void setReplies(ArrayList<CommentReply> replies) {
        this.replies = replies;
    }
}