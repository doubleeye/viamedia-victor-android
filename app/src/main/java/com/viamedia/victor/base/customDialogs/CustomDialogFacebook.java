package com.viamedia.victor.base.customDialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.viamedia.victor.base.LoginActivity;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

import java.util.Locale;

public class CustomDialogFacebook extends Dialog {
    private static final String TAG = "CustomDialogFacebook";
    private Activity activity;
    private Dialog dialog;
    private Button submitButton, cancelButton;
    private EditText txtMsisdn, age, city, country;
    private AppCompatRadioButton radioMale, radioFemale;
    private String facebookId, facebookFirstName, facebookLastName, facebookEmail, facebookToken, facebookAge, facebookGenderShort, avatar, facebookProfilePictureUri;
    private User user;
    private String formattedNumberWithPlus;
    private Util util;
    private ImageUtil imageUtil;
    private boolean formSumbitted = false;

    public CustomDialogFacebook(
            Activity activity,
            final String facebookId,
            final String facebookFirstName,
            final String facebookLastName,
            final String facebookEmail,
            final String facebookToken,
            final String facebookAge,
            final String facebookGenderShort,
            final User user,
            String facebookProfilePictureUri) {

        super(activity);
        this.activity = activity;
        this.dialog = this;

        this.facebookId = facebookId;
        this.facebookFirstName = facebookFirstName;
        this.facebookLastName = facebookLastName;
        this.facebookEmail = facebookEmail;
        this.facebookToken = facebookToken;
        this.facebookAge = facebookAge;
        this.facebookGenderShort = facebookGenderShort;
        this.facebookProfilePictureUri = facebookProfilePictureUri;
        this.user = user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_facebook);

        initManagers();
        initUi();
        loadData();
    }

    private void initUi() {
        submitButton = (Button) findViewById(R.id.positive_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        txtMsisdn = (EditText) findViewById(R.id.phone_number);
        radioMale = (AppCompatRadioButton) findViewById(R.id.radio_button_male);
        radioFemale = (AppCompatRadioButton) findViewById(R.id.radio_button_female);
        age = (EditText) findViewById(R.id.age);
        city = (EditText) findViewById(R.id.city);
        country = (EditText) findViewById(R.id.country);

        uiMsisdnTextChangeValidation();

        // actions
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitButtonClick(v);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                ((LoginActivity) activity).facebookLogout();
            }
        });
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        imageUtil = MyApp.getInstance().getImageUtil();
    }

    private void loadData() {
        // default/fallback
        // get facebook avatar
        if (facebookId != null && facebookProfilePictureUri != null) {
            Glide.with(activity)
                    .load(facebookProfilePictureUri)
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            // bitmap loaded into memory
                            avatar = imageUtil.encodeToBase64(resource, Bitmap.CompressFormat.JPEG);
                            if (formSumbitted) {
                                // registration was put on hold to wait for this response
                                apiFacebookRegister();
                            }
                        }
                    });
        }

        // prepop msisdn
        txtMsisdn.setText(
                String.format(
                        Locale.getDefault(),
                        activity.getString(R.string.msisdn_with_plus),
                        MyApp.getInstance().getUserManager().getCountryDialCode()));

        if (user != null && user.getMsisdnWithoutPlus() != null && user.getMsisdnWithoutPlus().length() > 0) {
            formattedNumberWithPlus = MyApp.getInstance().getValidationManager().isPhoneNumberValid(user.getMsisdnWithoutPlus());
            if (formattedNumberWithPlus == null) {
                formattedNumberWithPlus = MyApp.getInstance().getValidationManager().isPhoneNumberValid(user.getMsisdnWithPlus());
            }

            if (formattedNumberWithPlus != null) {
                txtMsisdn.setText(formattedNumberWithPlus);
            }
        }

        // update cursor position of first field
        txtMsisdn.setSelection(txtMsisdn.getText().length());

        // prepop age
        if (facebookAge != null) {
            age.setText(facebookAge);
        }

        // prepop gender
        if (facebookGenderShort != null) {
            if (facebookGenderShort.equalsIgnoreCase(Constants.M)) {
                radioMale.setChecked(true);
            }
            if (facebookGenderShort.equalsIgnoreCase(Constants.F)) {
                radioFemale.setChecked(true);
            }
        }
    }

    private void uiMsisdnTextChangeValidation() {
        txtMsisdn.addTextChangedListener(
                new PhoneNumberFormattingTextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // submitValidation
                        formattedNumberWithPlus = MyApp.getInstance().getValidationManager().isPhoneNumberValid(String.valueOf(s));
                        if (formattedNumberWithPlus != null) {
                            submitButton.setEnabled(true);
                        } else {
                            submitButton.setEnabled(false);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                });
    }

    private void submitButtonClick(View v) {
        if (MyApp.getInstance().getValidationManager().isFacebookLoginDialogFormValid(
                findViewById(android.R.id.content),
                age,
                city,
                country)) {

            formSumbitted = true;

            // register a new facebook user via our api
            if (avatar != null) {
                apiFacebookRegister();
            } else {
                showLoadingDialog();
            }
        }
    }

    private void apiFacebookRegister() {
        // update login edit text msisdn
        ((LoginActivity) activity).getMsisdnEditText().setText(formattedNumberWithPlus);
        dialog.dismiss();

        ((LoginActivity) activity).apiFacebookRegister(
                util.removePlusFromNumber(formattedNumberWithPlus),
                facebookId,
                facebookFirstName,
                facebookLastName,
                facebookEmail,
                facebookToken,
                radioMale.isChecked() ? Constants.M : Constants.F,
                String.valueOf(age.getText()),
                String.valueOf(city.getText()),
                String.valueOf(country.getText()),
                avatar);
    }

    private void hideLoadingDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                if (loadingLayout != null) {
                    loadingLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showLoadingDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                if (loadingLayout != null) {
                    loadingLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}