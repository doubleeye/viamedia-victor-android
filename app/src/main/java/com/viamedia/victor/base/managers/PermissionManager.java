package com.viamedia.victor.base.managers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;

public class PermissionManager {
    /* dangerous permissions and permission groups
    https://developer.android.com/guide/topics/security/permissions.html
    */

    /* request permission
    // If the user accepts one permission in a group/category they accept the entire group
    if (permissionManager.hasPermission(context, Constants.PERMISSION_READ_EXTERNAL_STORAGE)) {
        // continue with normal operation
    } else {
        if (permissionManager.shouldWeAsk(context, Constants.PERMISSION_READ_EXTERNAL_STORAGE)) {
            String[] perms = {Constants.PERMISSION_READ_EXTERNAL_STORAGE};
            permissionManager.askPermissions(activity, 200, perms);
        } else {
            // inform user that permission was denied
            Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_denied_by_user, Snackbar.LENGTH_LONG).show();
        }
    }
    */

    /* reading permission response
    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                boolean perm1 = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (perm1) {
                    // If the user accepts one permission in a group/category they accept the entire group
                    Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_successfully_granted, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }
    */

    public void askPermissions(Activity activity, int permsRequestCode, String[] perms) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(perms, permsRequestCode);

            // mark already requested perms
            for (String perm : perms) {
                markAsAsked(activity, perm);
            }
        }
    }

    public boolean shouldWeAsk(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            return (prefs.getBoolean(permission, true));
        }
        return false;
    }

    private void markAsAsked(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            prefs.edit().putBoolean(permission, false).apply();
        }
    }

    public boolean hasPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }
}