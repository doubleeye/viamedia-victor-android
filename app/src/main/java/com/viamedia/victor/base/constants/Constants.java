package com.viamedia.victor.base.constants;

import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.api.apiModels.UserInterest;

import java.util.ArrayList;

public class Constants {
    public static String SHARED_PREF_COUNTRYCODE = "user_country";

    public static String EXTRA_SEARCH_STRING = "extra_search_string";

    public static String EXTRA_ARTICLE_COMMENTS_ENABLED = "extra_article_comments_enabled";
    public static String EXTRA_ARTICLE_TITLE = "extra_article_title";
    public static String EXTRA_ARTICLE_HTML_CONTENT = "extra_article_html";
    public static String EXTRA_ARTICLE_IMAGE_URL = "extra_article_image_url";
    public static String EXTRA_ARTICLE_ID = "extra_article_id";
    public static String EXTRA_ARTICLE_URL = "extra_article_url";
    public static String EXTRA_RESET_PASSWORD_OTP = "extra_reset_password_otp";
    public static String EXTRA_RESET_PASSWORD_CUSTOM_PROFILE = "extra_reset_password_custom_profile";

    public static String EXTRA_INTERESTS_CHECKED = "extra_interests_checked";
    public static String EXTRA_INTERESTS_UNCHECKED = "extra_interests_unchecked";

    public static String EXTRA_REGISTRATION_SUCCESS = "extra_registration_success";
    public static String EXTRA_LOGIN_SUCCESS = "extra_login_success";

    public static String EXTRA_MSISDN_WITHOUT_PLUS = "extra_msisdn_without_plus";

    public static String MALE = "male";
    public static String FEMALE = "female";
    public static String M = "m";
    public static String F = "f";

    public static String EXTRA_VOUCHER_ID = "extra_voucher_id";
    public static String EXTRA_VOUCHER_TITLE = "extra_voucher_title";
    public static String EXTRA_VOUCHER_IMAGE_URL = "extra_voucher_image_url";
    public static String EXTRA_VOUCHER_HTML_CONTENT = "extra_voucher_html";

    public static String EXTRA_TABBED_CATEGORY_TITLE = "extra_tabbed_category_title";
    public static String EXTRA_TABBED_CATEGORY_SECTION_ID = "extra_tabbed_category_section_id";

    public static String REQUEST_URL_IGNORE_SIGN_OUT_FACEBOOK_LOGIN = "/users/facebook-login";
    public static String REQUEST_URL_IGNORE_SIGN_OUT_CUSTOM_LOGIN = "/users/login";
    public static String REQUEST_URL_IGNORE_SIGN_OUT_CUSTOM_REGISTRATION = "/users";
    public static String REQUEST_URL_IGNORE_SIGN_OUT_PASSWORD_RESET_PART1 = "/users/";
    public static String REQUEST_URL_IGNORE_SIGN_OUT_PASSWORD_RESET_PART2 = "/edit";

    public static String RESPONSE_TYPE_APPLICATION_JSON = "application/json";
    public static int API_THREADPOOL_SIZE = 10;

    public static String ENCODING_HTML = "text/html; charset=UTF-8";

    public static String PRICING_STATUS_PAID = "paid";
    public static String PRICING_STATUS_UNPAID = "unpaid";
    public static String PRICING_STATUS_PENDING = "pending";

    public static String PRICING_STRATEGY_TYPE_STOREFRONT = "storefront"; // whole storefront, all content
    public static String PRICING_STRATEGY_TYPE_SECTION = "section"; // top level section of storefront
    public static String PRICING_STRATEGY_TYPE_SELECTION = "selection"; // sub section with a list of articles

    public static String PAID_CONTENT_BILLLING_CHOICE_AD_HOC = "Ad Hoc";
    public static String PAID_CONTENT_BILLLING_CHOICE_SUBSCRIPTION = "Subscription";

    public static String PERMISSION_READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    public static String PERMISSION_ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";

    public static String GCM_SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static String GCM_CLIENT_TOKEN = "gcmClientToken";
    public static String GCM_REGISTRATION_COMPLETE = "registrationComplete";
    public static int GCM_PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static String SECTION_HOME_FEED = "Home";

    public static final int REQUEST_CODE_SELECT_PHOTO_GALLERY = 111;
    public static final int REQUEST_CODE_SELECT_PHOTO_CAMERA = 112;
    public static int IMAGE_UPLOAD_QUALITY = 80;

    public static String RESPONSE_ERROR_CODE_VALIDATION_FAILED = "VALIDATION_FAILED";

    public static String RESPONSE_ERROR_MESSAGE_NO_USER_FOUND = "NO USER FOUND";
    public static String RESPONSE_ERROR_MESSAGE_AUTHENTICATION_FAILED = "AUTHENTICATION FAILED";
    public static String RESPONSE_ERROR_MESSAGE_USER_NOT_FOUND = "USER NOT FOUND";

    public static String ARTICLE_TYPE_AUDIO = "audio";
    public static String ARTICLE_TYPE_GALLERY = "gallery";
    public static String ARTICLE_TYPE_IMAGE = "image";
    public static String ARTICLE_TYPE_VIDEO = "video";

    public static String SECTION_STATIC_HOME = "home";

    public static ArrayList<UserInterest> interests = new ArrayList<UserInterest>() {{
        add(new UserInterest(1, MyApp.getContext().getResources().getString(R.string.environment)));
        add(new UserInterest(2, MyApp.getContext().getResources().getString(R.string.finance)));
        add(new UserInterest(3, MyApp.getContext().getResources().getString(R.string.fitness)));
        add(new UserInterest(4, MyApp.getContext().getResources().getString(R.string.games)));
        add(new UserInterest(5, MyApp.getContext().getResources().getString(R.string.health)));
        add(new UserInterest(6, MyApp.getContext().getResources().getString(R.string.motoring)));
        add(new UserInterest(7, MyApp.getContext().getResources().getString(R.string.music)));
        add(new UserInterest(8, MyApp.getContext().getResources().getString(R.string.opinions)));
        add(new UserInterest(9, MyApp.getContext().getResources().getString(R.string.politics)));
        add(new UserInterest(10, MyApp.getContext().getResources().getString(R.string.sport)));
        add(new UserInterest(11, MyApp.getContext().getResources().getString(R.string.travel)));
        add(new UserInterest(12, MyApp.getContext().getResources().getString(R.string.technology)));
    }};

    public static ArrayList<String> interestThumbs = new ArrayList<String>() {{
        add(String.valueOf(R.drawable.interest_environment));
        add(String.valueOf(R.drawable.interest_finance));
        add(String.valueOf(R.drawable.interest_fitness));
        add(String.valueOf(R.drawable.interest_games));
        add(String.valueOf(R.drawable.interest_health));
        add(String.valueOf(R.drawable.interest_motoring));
        add(String.valueOf(R.drawable.interest_music));
        add(String.valueOf(R.drawable.interest_opinions));
        add(String.valueOf(R.drawable.interest_politics));
        add(String.valueOf(R.drawable.interest_sport));
        add(String.valueOf(R.drawable.interest_travel));
        add(String.valueOf(R.drawable.interest_technology));
    }};
}