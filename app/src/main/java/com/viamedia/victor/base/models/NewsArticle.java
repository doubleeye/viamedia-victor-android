package com.viamedia.victor.base.models;

public class NewsArticle {
    private String articleTitle;
    private String articleBlurb;
    private int articleThumbnail;
    private int articleImage;
    private String articleDate;
    private String articleBody;

    public NewsArticle(String articleTitle, String articleBlurb, int articleThumbnail, int articleImage, String articleDate, String articleBody) {
        this.articleTitle = articleTitle;
        this.articleBlurb = articleBlurb;
        this.articleThumbnail = articleThumbnail;
        this.articleImage = articleImage;
        this.articleDate = articleDate;
        this.articleBody = articleBody;

    }

    public NewsArticle() {

    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleBlurb() {
        return articleBlurb;
    }

    public void setArticleBlurb(String articleBlurb) {
        this.articleBlurb = articleBlurb;
    }

    public int getArticleThumbnail() {
        return articleThumbnail;
    }

    public void setArticleThumbnail(int articleThumbnail) {
        this.articleThumbnail = articleThumbnail;
    }


    public String getArticleBody() {
        return articleBody;
    }

    public void setArticleBody(String articleBody) {
        this.articleBody = articleBody;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public int getArticleImage() {
        return articleImage;
    }

    public void setArticleImage(int articleImage) {
        this.articleImage = articleImage;
    }


}
