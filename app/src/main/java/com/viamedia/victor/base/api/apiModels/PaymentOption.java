package com.viamedia.victor.base.api.apiModels;

public class PaymentOption {
    private int option_id;
    private String option_name;
    private String option_type;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOptionId() {
        return option_id;
    }

    public void setOptionId(int option_id) {
        this.option_id = option_id;
    }

    public String getOptionName() {
        return option_name;
    }

    public void setOptionName(String option_name) {
        this.option_name = option_name;
    }

    public String getOptionType() {
        return option_type;
    }

    public void setOptionType(String option_type) {
        this.option_type = option_type;
    }
}
