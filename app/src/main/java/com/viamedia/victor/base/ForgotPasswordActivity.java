package com.viamedia.victor.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.managers.ValidationManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.Util;

import java.io.File;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
    private static final String TAG = "ForgotPasswordActivity";
    private UserManager userManager;
    private Util util;
    private EditText txtMsisdn;
    private Button submitButton;
    private StorageManager storageManager;
    private WebService webService;
    private ValidationManager validationManager;
    private User user;
    private String formattedNumber;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = this;

        initManagers();
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadUserData();
    }

    @Override
    protected void onPause() {
        saveUserData();
        super.onPause();
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        userManager = MyApp.getInstance().getUserManager();
        storageManager = MyApp.getInstance().getStorageManager();
        validationManager = MyApp.getInstance().getValidationManager();
        webService = MyApp.getInstance().getWebService();
    }

    private void initUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.forgot_password);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        txtMsisdn = (EditText) findViewById(R.id.msisdn);
        submitButton = (Button) findViewById(R.id.okay_button);

        // actions
        uiMsisdnTextChangeValidation();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // reset password
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValidation();
            }
        });
    }

    private void uiMsisdnTextChangeValidation() {
        txtMsisdn.addTextChangedListener(
                new PhoneNumberFormattingTextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // submitValidation
                        formattedNumber = validationManager.isPhoneNumberValid(String.valueOf(s));
                        if (formattedNumber != null) {
                            submitButton.setEnabled(true);
                        } else {
                            submitButton.setEnabled(false);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                }
        );
    }

    private void submitValidation() {
        user.setMsisdnWithoutPlus(formattedNumber);
        apiRequestPasswordReset(user.getMsisdnWithoutPlus());
    }

    private void saveUserData() {
        user = storageManager.getUserObject();
        if (user == null) {
            user = new User();
        }

        user.setMsisdnWithoutPlus(formattedNumber);
        storageManager.saveDataToFile(
                new File(storageManager.getUserFilename()),
                (new Gson()).toJson(user)
        );
    }

    private void loadUserData() {
        // default/fallback
        txtMsisdn.setText(
                String.format(
                        Locale.getDefault(),
                        getString(R.string.msisdn_with_plus),
                        MyApp.getInstance().getUserManager().getCountryDialCode()));

        user = storageManager.getUserObject();
        if (user != null) {
            if (user.getMsisdnWithoutPlus() != null && user.getMsisdnWithoutPlus().length() > 0) {
                formattedNumber = validationManager.isPhoneNumberValid(user.getMsisdnWithoutPlus());
                if (formattedNumber == null) {
                    formattedNumber = validationManager.isPhoneNumberValid(user.getMsisdnWithPlus());
                }

                if (formattedNumber != null) {
                    txtMsisdn.setText(formattedNumber);
                }
            }
        }

        // update cursor position of first field
        txtMsisdn.setSelection(txtMsisdn.getText().length());
    }

    private void apiRequestPasswordReset(final String msisdn) {
        showLoading();

        Call<ResponseBase> call = webService.requestPasswordReset(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                getResources().getString(R.string.api_version_string),
                msisdn
        );
        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response != null && response.body() != null && response.body().isSuccess()) {
                    Snackbar.make(findViewById(android.R.id.content), R.string.reset_instructions_has_been_sent, Snackbar.LENGTH_LONG).show();
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Intent intent = new Intent(context, ForgotPasswordOtpActivity.class);
                            intent.putExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS, user.getMsisdnWithoutPlus());
                            startActivity(intent);
                            finish();
                        }
                    });
                    t.start();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.could_not_reset_password_please_try_again, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.could_not_reset_password_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }
}