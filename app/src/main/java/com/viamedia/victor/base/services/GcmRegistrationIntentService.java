/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.viamedia.victor.base.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.constants.Constants;

import java.io.IOException;

public class GcmRegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentSer";
    private static final String[] TOPICS = {"global"};

    public GcmRegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // generate new device client id/token
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(
                    getString(R.string.gcm_application_id),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                    null
            );
            Log.i(TAG, "GCM Registration Token: " + token);

            // notify 3rd party server of new device token
//            sendRegistrationToThirdPartyServer(token ,sharedPreferences);
            // subscribe to topic channels
//            subscribeTopics(token);

            sharedPreferences.edit().putString(Constants.GCM_CLIENT_TOKEN, token).apply();
            // [END register_for_gcm]
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Constants.GCM_REGISTRATION_COMPLETE));
    }

    private void sendRegistrationToThirdPartyServer(String token, SharedPreferences sharedPreferences) {
        // Add custom implementation, as needed.
        sharedPreferences.edit().putBoolean(Constants.GCM_SENT_TOKEN_TO_SERVER, true).apply();
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}