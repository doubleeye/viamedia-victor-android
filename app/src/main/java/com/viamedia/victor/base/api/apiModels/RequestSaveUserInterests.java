package com.viamedia.victor.base.api.apiModels;

import java.util.ArrayList;

public class RequestSaveUserInterests {
    private String token;
    private ArrayList<UserInterest> user_interests;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<UserInterest> getUserInterests() {
        return user_interests;
    }

    public void setUserInterests(ArrayList<UserInterest> user_interests) {
        this.user_interests = user_interests;
    }
}