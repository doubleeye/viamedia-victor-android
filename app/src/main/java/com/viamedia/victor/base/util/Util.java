package com.viamedia.victor.base.util;

import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.text.format.DateUtils;
import android.util.Log;

import com.viamedia.victor.base.api.apiModels.PaymentOption;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.customDialogs.CustomDialogGps;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Util {
    private static final String TAG = "Util";

    public String removePlusFromNumber(String number) {
        return number.replace("+", "");
    }

    public String getNameFromContentSelectionUrl(String url) {
        if (url != null) {
            int index = url.lastIndexOf("/");
            return url.substring(index + 1, url.length());
        }
        return "";
    }

    public String toTitleCase(String s) {
        if (s != null && s.length() > 0) {
            String newString = String.valueOf(Character.toTitleCase(s.charAt(0)));

            if (s.length() > 1) {
                newString += s.substring(1, s.length());
            }
            return newString;
        }

        return null;
    }

    public Date parseTime(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String timeZone = time.substring(time.lastIndexOf(" ") + 1, time.length());
        format.setTimeZone(TimeZone.getTimeZone(timeZone));
        try {
            return format.parse(time);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    public String getNiceTime(Date dateFrom) {
        long fromEpochMs = dateFrom.getTime();
        long toEpochMs = System.currentTimeMillis();

        if (toEpochMs - fromEpochMs > DateUtils.HOUR_IN_MILLIS) {
            // long date
            return new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(fromEpochMs);
        } else {
            // short date
            return String.valueOf(
                    DateUtils.getRelativeTimeSpanString(
                            fromEpochMs,
                            toEpochMs,
                            0));
        }
    }

    public String getAgeFromFacebookBirthday(String facebookBirthday) {
        // possible formats
        // MM/DD/YYYY
        // YYYY
        // MM/DD

        if (facebookBirthday != null) {
            // try to extract age from facebook birthday string
            int count = facebookBirthday.length() - facebookBirthday.replace("/", "").length();
            if (count == 0 || count == 2) {
                int currentYear = Calendar.getInstance().get(Calendar.YEAR);

                // has the year
                if (count == 0) {
                    try {
                        int birthYear = Integer.parseInt(facebookBirthday);
                        return String.valueOf(currentYear - birthYear);
                    } catch (NumberFormatException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
                if (count == 2) {
                    try {
                        String birthYearString = facebookBirthday.substring(facebookBirthday.lastIndexOf("/") + 1, facebookBirthday.length());
                        int birthYear = Integer.parseInt(birthYearString);
                        return String.valueOf(currentYear - birthYear);
                    } catch (NumberFormatException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            } else {
                // no year was found
            }
        }

        // no age was found
        return null;
    }

    public String getGenderFromFacebookGender(String gender) {
        if (gender != null) {
            if (gender.toLowerCase(Locale.getDefault()).contains(Constants.F)) {
                // need to be first, female includes male
                return Constants.F;
            } else if (gender.toLowerCase(Locale.getDefault()).contains(Constants.M)) {
                return Constants.M;
            }
        }
        return null;
    }

    public void locationCheck(final Activity activity) {
        LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CustomDialogGps customDialogGps = new CustomDialogGps(activity);
                    customDialogGps.show();
                }
            });
        }
    }

    public boolean isDefaultPaymentOption(PaymentOption paymentOption) {
        // TODO: Server needs to specify fallback payment options
        if (paymentOption.getOptionType().equalsIgnoreCase("Ad Hoc")) {
            return true;
        }

        return false;
    }
}