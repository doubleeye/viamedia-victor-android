package com.viamedia.victor.base.models;

public class InterestLocal {
    private int id;
    private String title;
    private boolean enabled;
    private String image;

    public InterestLocal() {
    }

    public InterestLocal(int id, String title, String image, boolean enabled) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.enabled = enabled;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
