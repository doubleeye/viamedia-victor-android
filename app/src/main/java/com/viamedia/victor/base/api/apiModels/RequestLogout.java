package com.viamedia.victor.base.api.apiModels;

public class RequestLogout {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
