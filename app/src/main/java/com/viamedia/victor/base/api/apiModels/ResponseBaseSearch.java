package com.viamedia.victor.base.api.apiModels;

import java.util.ArrayList;

public class ResponseBaseSearch extends ResponseBase {
    private ArrayList<SearchItem> data;

    public ArrayList<SearchItem> getData() {
        return data;
    }

    public void setData(ArrayList<SearchItem> data) {
        this.data = data;
    }

    public class SearchItem {
        private String id;
        private String content;
        private String url;
        private String description;
        private String title;
        private String created_at;
        private String modified_at;
        private FeaturedImage featured_image;
        private PricingStrategy pricing_strategy;
        private ArrayList<PaymentOption> payment_options;
        private ArticleContentType content_types;

        public ArticleContentType getContentTypes() {
            return content_types;
        }

        public String getModifiedAt() {
            return modified_at;
        }

        public void setModifiedAt(String modified_at) {
            this.modified_at = modified_at;
        }

        public void setContentTypes(ArticleContentType content_types) {
            this.content_types = content_types;
        }

        public ArrayList<PaymentOption> getPaymentOptions() {
            return payment_options;
        }

        public void setPaymentOptions(ArrayList<PaymentOption> payment_options) {
            this.payment_options = payment_options;
        }

        public PricingStrategy getPricingStrategy() {
            return pricing_strategy;
        }

        public void setPricingStrategy(PricingStrategy pricing_strategy) {
            this.pricing_strategy = pricing_strategy;
        }

        public FeaturedImage getFeaturedImage() {
            return featured_image;
        }

        public void setFeaturedImage(FeaturedImage featured_image) {
            this.featured_image = featured_image;
        }

        public void setCreatedAt(String created_at) {
            this.created_at = created_at;
        }

        public String getCreatedAt() {
            return created_at;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}