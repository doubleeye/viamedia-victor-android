package com.viamedia.victor.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.WebServiceBuilder;
import com.viamedia.victor.base.managers.PermissionManager;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.managers.ValidationManager;
import com.viamedia.victor.base.services.GpsService;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

public class MyApp extends Application {
    private static final String TAG = "MyApp";
    private static MyApp singleton;
    private Context context;
    private WebService webService;
    private CallbackManager callbackManager;
    private Util util;
    private ImageUtil imageUtil;
    private UserManager userManager;
    private ValidationManager validationManager;
    private StorageManager storageManager;
    private PermissionManager permissionManager;

    public static MyApp getInstance() {
        return singleton;
    }

    public static Context getContext() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        context = singleton;

        Fabric.with(this, new Crashlytics());
        printApplicationKeyHash();

        WebServiceBuilder webServiceBuilder = new WebServiceBuilder();
        webService = webServiceBuilder.build(singleton);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        util = new Util();
        imageUtil = new ImageUtil();
        userManager = new UserManager(singleton);
        storageManager = new StorageManager(singleton);
        validationManager = new ValidationManager();
        permissionManager = new PermissionManager();
    }

    public PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public ValidationManager getValidationManager() {
        return validationManager;
    }

    public WebService getWebService() {
        return webService;
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public Util getUtil() {
        return util;
    }

    public ImageUtil getImageUtil() {
        return imageUtil;
    }

    private void printApplicationKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES
            );

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public void startGpsService() {
        startService(new Intent(singleton, GpsService.class));
    }

    public void stopGpsService() {
        stopService(new Intent(singleton, GpsService.class));
    }
}