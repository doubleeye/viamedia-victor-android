package com.viamedia.victor.base.api;

import android.content.Context;

import com.viamedia.victor.base.R;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebServiceBuilder {
    public static WebService build(Context context) {
        // custom interceptor, for global responses and log level
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // custom ok http client
        final OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(context.getResources().getInteger(R.integer.retrofit_timeout_seconds), TimeUnit.SECONDS)
                .connectTimeout(context.getResources().getInteger(R.integer.retrofit_timeout_seconds), TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new ResponseInterceptor())
                .build();

        // builder
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(String.format("http://%1$s:%2$s",
                        context.getResources().getString(R.string.base_url),
                        context.getResources().getString(R.string.base_url_port)
                ))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return builder.create(WebService.class);
    }
}