package com.viamedia.victor.base;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestPasswordReset;
import com.viamedia.victor.base.api.apiModels.RequestUpdateUser;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.api.apiModels.ResponseBaseLoginRegistration;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.ValidationManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    private static final String TAG = "ChangePasswordActivity";
    private Button submitButton;
    private EditText password, passwordConfirmation;
    private WebService webService;
    private ValidationManager validationManager;
    private StorageManager storageManager;
    private String otp;
    private String msisdnWithoutPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        if (getIntent().hasExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS)) {
            if (getIntent().hasExtra(Constants.EXTRA_RESET_PASSWORD_OTP)) {
                otp = getIntent().getStringExtra(Constants.EXTRA_RESET_PASSWORD_OTP);
            }
            msisdnWithoutPlus = getIntent().getStringExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS);

            initManagers();
            initUi();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(Constants.EXTRA_RESET_PASSWORD_CUSTOM_PROFILE, false)) {
            // not logged in
            Intent intent = new Intent(this, ForgotPasswordOtpActivity.class);
            intent.putExtra(Constants.EXTRA_RESET_PASSWORD_OTP, otp);
            intent.putExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS, msisdnWithoutPlus);
            startActivity(intent);
        }
        super.onBackPressed();
    }

    private void initUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.change_password);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        password = (EditText) findViewById(R.id.password);
        passwordConfirmation = (EditText) findViewById(R.id.confirm_password);
        submitButton = (Button) findViewById(R.id.create_button);

        // actions
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValidation();
            }
        });
    }

    private void initManagers() {
        webService = MyApp.getInstance().getWebService();
        validationManager = MyApp.getInstance().getValidationManager();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    public void submitValidation() {
        if (validationManager.isChangePasswordFormValid(
                findViewById(android.R.id.content),
                password,
                passwordConfirmation)) {

            if (getIntent().getBooleanExtra(Constants.EXTRA_RESET_PASSWORD_CUSTOM_PROFILE, false)) {
                // logged in
                apiUpdateUser(String.valueOf(password.getText()), String.valueOf(passwordConfirmation.getText()));
            } else {
                // not logged in
                apiPasswordReset(
                        msisdnWithoutPlus,
                        otp,
                        String.valueOf(password.getText()),
                        String.valueOf(passwordConfirmation.getText()));
            }
        }
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void apiPasswordReset(
            final String msisdn,
            final String otp,
            final String password,
            final String confirmPassword) {

        showLoading();

        final RequestPasswordReset requestPasswordReset = new RequestPasswordReset();
        requestPasswordReset.setOtp(otp);
        requestPasswordReset.setPassword(password);
        requestPasswordReset.setPasswordConfirmation(confirmPassword);

        Call<ResponseBase> call = webService.resetPassword(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                getResources().getString(R.string.api_version_string),
                msisdn,
                requestPasswordReset);

        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response.code() >= 200 && response.code() <= 299) {
                    Snackbar.make(findViewById(android.R.id.content), R.string.your_password_has_been_changed_successfully, Snackbar.LENGTH_LONG).show();

                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    });
                    t.start();
                } else {
                    // reset has failed
                    Snackbar.make(findViewById(android.R.id.content), R.string.could_not_reset_password_please_check_the_otp, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.change_password_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void apiUpdateUser(
            final String password,
            final String confirmationPassword) {

        showLoading();

        final RequestUpdateUser requestUpdateUser = new RequestUpdateUser();
        requestUpdateUser.setToken(storageManager.getUserObject().getLoginToken());
        requestUpdateUser.setDeviceCode(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.GCM_CLIENT_TOKEN, null));
        requestUpdateUser.setPassword(password);
        requestUpdateUser.setPasswordConfirmation(confirmationPassword);

        Call<ResponseBaseLoginRegistration> call = webService.update(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                msisdnWithoutPlus,
                requestUpdateUser
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response.code() >= 200 && response.code() <= 299) {
                    Snackbar.make(findViewById(android.R.id.content), R.string.your_password_has_been_changed_successfully, Snackbar.LENGTH_LONG).show();
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    });
                    t.start();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.change_password_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.change_password_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }
}