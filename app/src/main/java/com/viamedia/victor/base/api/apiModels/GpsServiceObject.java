package com.viamedia.victor.base.api.apiModels;

public class GpsServiceObject {
    private double lat;
    private double lng;

    public GpsServiceObject(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public String getLatString() {
        return String.valueOf(lat);
    }

    public String getLngString() {
        return String.valueOf(lng);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
