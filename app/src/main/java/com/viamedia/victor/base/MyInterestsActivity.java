package com.viamedia.victor.base;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.viamedia.victor.base.adapters.GridSpacingItemDecoration;
import com.viamedia.victor.base.adapters.MyInterestsAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestSaveUserInterests;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.api.apiModels.UserInterest;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.InterestLocal;
import com.viamedia.victor.base.models.InterestsLocal;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyInterestsActivity extends AppCompatActivity {
    private static final String TAG = "MyInterestsActivity";
    private Context context;
    private RecyclerView recyclerView;
    private MyInterestsAdapter adapter;
    private StorageManager storageManager;
    private WebService webService;
    private InterestsLocal data;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_interests);
        context = this;

        initManagers();
        initUi();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (data.getInterestsLocalSelected().size() >= context.getResources().getInteger(R.integer.min_interests)) {
            saveData();
        }
        super.onPause();
    }

    private void saveData() {
        apiSaveUserInterests(
                storageManager.getUserObject().getLoginToken(),
                storageManager.getUserObject().getMsisdnWithoutPlus(),
                data.getInterestsLocalSelected()
        );
    }

    private void initManagers() {
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void initUi() {
        int spanCount = 3; // column count
        int spacing = 5; // column spacing
        boolean includeEdge = false; // edge space

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void loadData() {
        // TODO: load from server
        data = storageManager.getInterestsObject();
        if (data == null) {
            data = new InterestsLocal();
            createStubData();
        }

        adapter = new MyInterestsAdapter(context, data, new MyInterestsAdapter.AdapterInterface() {
            @Override
            public void interfaceResponse(int position, boolean isChecked) {
                data.getInterestsLocal().get(position).setEnabled(isChecked);
                checkMinInterests();
            }
        });
        recyclerView.setAdapter(adapter);
        checkMinInterests();
    }

    private void checkMinInterests() {
        if (data.getInterestsLocalSelected().size() < context.getResources().getInteger(R.integer.min_interests)) {
            if (snackbar == null || !snackbar.isShown()) {
                snackbar = Snackbar.make(
                        findViewById(android.R.id.content),
                        String.format(getString(R.string.please_select_a_min_of_interests), getResources().getInteger(R.integer.min_interests)),
                        Snackbar.LENGTH_INDEFINITE
                ).setAction(R.string.OK, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
                snackbar.show();
            }
        } else {
            if (snackbar != null && snackbar.isShown()) {
                snackbar.dismiss();
            }
        }
    }

    private void createStubData() {
        ArrayList<InterestLocal> interestLocalArrayList = new ArrayList<>();
        ArrayList<String> interestThumbs = Constants.interestThumbs;
        ArrayList<UserInterest> interests = Constants.interests;

        for (int i = 0; i < interests.size(); i++) {
            interestLocalArrayList.add(new InterestLocal(
                    interests.get(i).getId(),
                    interests.get(i).getName(),
                    interestThumbs.get(i),
                    false
            ));
        }
        data.setInterestsLocal(interestLocalArrayList);
    }

    private void apiSaveUserInterests(
            final String token,
            final String msisdn,
            final ArrayList<InterestLocal> interestsLocalSelected) {

        ArrayList<UserInterest> userInterests = new ArrayList<>();
        for (int i = 0; i < interestsLocalSelected.size(); i++) {
            userInterests.add(new UserInterest(
                    interestsLocalSelected.get(i).getId(),
                    interestsLocalSelected.get(i).getTitle()
            ));
        }

        final RequestSaveUserInterests requestSaveUserInterests = new RequestSaveUserInterests();
        requestSaveUserInterests.setToken(token);
        requestSaveUserInterests.setUserInterests(userInterests);

        Call<ResponseBase> call = webService.saveUserInterests(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                msisdn,
                requestSaveUserInterests
        );
        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");
                Snackbar.make(findViewById(android.R.id.content), R.string.saved, Snackbar.LENGTH_SHORT).show();

                if (storageManager != null && data != null) {
                    storageManager.saveDataToFile(
                            new File(storageManager.getInterestsFilename()),
                            new Gson().toJson(data)
                    );
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}