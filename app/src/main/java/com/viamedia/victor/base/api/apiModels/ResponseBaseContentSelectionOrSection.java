package com.viamedia.victor.base.api.apiModels;

import java.util.ArrayList;

public class ResponseBaseContentSelectionOrSection extends ResponseBase {
    private DataObject data;

    public DataObject getData() {
        return data;
    }

    public void setData(DataObject data) {
        this.data = data;
    }

    public class DataObject {
        private int page;
        private int limit;
        private int count;
        private int totalItems;
        private int pages;
        private ResponseContentSelectionResult results;

        public class ResponseContentSelectionResult {
            private int id;
            private String slug;
            private String url;
            private ArrayList<ResponseContentSelectionResultContentItem> content_items;

            public class ResponseContentSelectionResultContentItem {
                private int id;
                private String content;
                private String url;
                private String label;
                private String description;
                private String keywords;
                private String title;
                private String created_at;
                private String modified_at;
                private FeaturedImage featured_image;
                private PricingStrategy pricing_strategy;
                private ArrayList<PaymentOption> payment_options;
                private ArticleContentType content_types;

                public ArticleContentType getContentTypes() {
                    return content_types;
                }

                public void setContentTypes(ArticleContentType content_types) {
                    this.content_types = content_types;
                }

                public ArrayList<PaymentOption> getPaymentOptions() {
                    return payment_options;
                }

                public void setPaymentOptions(ArrayList<PaymentOption> payment_options) {
                    this.payment_options = payment_options;
                }

                public PricingStrategy getPricingStrategy() {
                    return pricing_strategy;
                }

                public void setPricingStrategy(PricingStrategy pricing_strategy) {
                    this.pricing_strategy = pricing_strategy;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getLabel() {
                    return label;
                }

                public void setLabel(String label) {
                    this.label = label;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getKeywords() {
                    return keywords;
                }

                public void setKeywords(String keywords) {
                    this.keywords = keywords;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public FeaturedImage getFeaturedImage() {
                    return featured_image;
                }

                public void setFeaturedImage(FeaturedImage featured_image) {
                    this.featured_image = featured_image;
                }

                public String getCreatedAt() {
                    return created_at;
                }

                public void setCreatedAt(String created_at) {
                    this.created_at = created_at;
                }

                public String getModifiedAt() {
                    return modified_at;
                }

                public void setModifiedAt(String modified_at) {
                    this.modified_at = modified_at;
                }
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public ArrayList<ResponseContentSelectionResultContentItem> getContentItems() {
                return content_items;
            }

            public void setContentItems(ArrayList<ResponseContentSelectionResultContentItem> content_items) {
                this.content_items = content_items;
            }
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getTotalItems() {
            return totalItems;
        }

        public void setTotalItems(int totalItems) {
            this.totalItems = totalItems;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public ResponseContentSelectionResult getResults() {
            return results;
        }

        public void setResults(ResponseContentSelectionResult results) {
            this.results = results;
        }
    }
}