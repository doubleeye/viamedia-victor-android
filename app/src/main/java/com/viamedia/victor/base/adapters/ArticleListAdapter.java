package com.viamedia.victor.base.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.api.apiModels.ResponseBaseSearch;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.models.ArticleLocal;
import com.viamedia.victor.base.util.DynamicImageView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder> {
    private static final String TAG = "ArticleListAdapter";
    private ArrayList<ArticleLocal> data;
    private AdapterInterface adapterInterface;
    private int layout = 1; // valid values [1,2]
    private Activity activity;
    private Context context;

    public ArticleListAdapter(
            Activity activity,
            ResponseBaseContentSelectionOrSection result,
            ResponseBaseSearch result2,
            int layout,
            AdapterInterface adapterInterface) {

        this.activity = activity;
        this.context = activity;
        this.adapterInterface = adapterInterface;
        if (layout >= 1 && layout <= 2) {
            this.layout = layout;
        }

        // parse result into data
        if (result != null) {
            final ArrayList<ArticleLocal> data = new ArrayList<>();
            if (result != null && result.getData() != null) {
                for (final ResponseBaseContentSelectionOrSection.DataObject.ResponseContentSelectionResult.ResponseContentSelectionResultContentItem item : result.getData().getResults().getContentItems()) {
                    data.add(new ArticleLocal(item, result.getData().getResults().getSlug()));
                }
            }
            this.data = data;
        }
        if (result2 != null) {
            final ArrayList<ArticleLocal> data = new ArrayList<>();
            for (final ResponseBaseSearch.SearchItem item : result2.getData()) {
                data.add(new ArticleLocal(item, ""));
            }
            this.data = data;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView title, description, price, time, paymentStatus;
        public ImageView typeImage;
        public DynamicImageView image;
        public ProgressBar imageProgress;

        public ViewHolder(final View itemView, final AdapterInterface adapterInterface) {
            super(itemView);
            mView = itemView;

            // ui
            title = (TextView) itemView.findViewById(R.id.item_title);
            price = (TextView) itemView.findViewById(R.id.item_price);
            paymentStatus = (TextView) itemView.findViewById(R.id.item_payment_status);
            description = (TextView) itemView.findViewById(R.id.category_adapter_item_description);
            image = (DynamicImageView) itemView.findViewById(R.id.category_adapter_item_image);
            typeImage = (ImageView) itemView.findViewById(R.id.category_adapter_item_type_image);
            time = (TextView) itemView.findViewById(R.id.item_time);
            imageProgress = (ProgressBar) itemView.findViewById(R.id.image_progress);

            // click activate or deactivate item
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterInterface.interfaceResponse(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public ArticleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        // layout style1
        if (layout == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_item_layout1, parent, false);
        }

        // layout style2
        if (layout == 2) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_item_layout2, parent, false);
        }

        return new ViewHolder(view, adapterInterface);
    }

    @Override
    public void onBindViewHolder(final ArticleListAdapter.ViewHolder holder, int position) {
        // default view
        holder.title.setText(data.get(position).getTitle());
        Date timeAsDate = data.get(position).getTimeAsDate();
        if (timeAsDate != null) {
            holder.time.setText(MyApp.getInstance().getUtil().getNiceTime(timeAsDate));
        }

        holder.description.setText(data.get(position).getDescription());

        // type
        if (data.get(position).getContentType() != null) {
            holder.typeImage.setVisibility(View.VISIBLE);

            if (data.get(position).getContentType().getSlug().equalsIgnoreCase(Constants.ARTICLE_TYPE_AUDIO)) {
                holder.typeImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_audio));
            } else if (data.get(position).getContentType().getSlug().equalsIgnoreCase(Constants.ARTICLE_TYPE_GALLERY)) {
                holder.typeImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_gallery));
            } else if (data.get(position).getContentType().getSlug().equalsIgnoreCase(Constants.ARTICLE_TYPE_IMAGE)) {
                holder.typeImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_image));
            } else if (data.get(position).getContentType().getSlug().equalsIgnoreCase(Constants.ARTICLE_TYPE_VIDEO)) {
                holder.typeImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_video));
            } else {
                holder.typeImage.setVisibility(View.GONE);
            }
        }

        // price
        if (data.get(position).getPricingStrategy() != null &&
                data.get(position).getPricingStrategy().getPrice() > 0 &&
                data.get(position).getPricingStrategy().getStatus() != null) {

            if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_PAID)) {
                // item has been paid for, hide price
                holder.price.setVisibility(View.GONE);
            } else if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_UNPAID)) {
                // item has not been paid for, show price
                holder.price.setVisibility(View.VISIBLE);
                // TODO: currency required from server
                holder.price.setText("R" + String.format(Locale.getDefault(), "%.2f", data.get(position).getPricingStrategy().getPrice()));
            } else if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_PENDING)) {
                // item has been paid, show pending
                holder.paymentStatus.setVisibility(View.VISIBLE);
                holder.paymentStatus.setText(R.string.payment_pending);
            }
        } else {
            // no payment required, hide price
            holder.price.setVisibility(View.GONE);
        }

        // load image from url
        Glide.clear(holder.image);
        Glide
                .with(context)
                .load(data.get(position).getImageUrl())
                .crossFade()
                .error(R.drawable.flavoured_category_banner) // fallback image
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        // hide image progress
                        if (holder.imageProgress != null) {
                            holder.imageProgress.setVisibility(View.GONE);
                        }

                        if (e != null) {
                            Log.e(TAG, e.getMessage());
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        // hide image progress
                        if (holder.imageProgress != null) {
                            holder.imageProgress.setVisibility(View.GONE);
                        }
                        return false;
                    }
                })
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public interface AdapterInterface {
        void interfaceResponse(int position);
    }

    public void addArticleList(ArrayList<ArticleLocal> data) {
        for (ArticleLocal item : data) {
            this.data.add(item);
        }
    }

    public void clearArticleList() {
        this.data = new ArrayList<>();
    }

    public ArrayList<ArticleLocal> getData() {
        return this.data;
    }
}