package com.viamedia.victor.base.api.apiModels;

import java.util.ArrayList;

public class ResponseBaseLoginRegistration extends ResponseBase {
    private DataObject data;

    public class DataObject {
        private int id;
        private String token;
        private String msisdn;
        private String email;
        private String first_name;
        private String last_name;
        private String facebook_user_id;
        private String city;
        private String country;
        private String gender;
        private String age;
        private String avatar;
        private ArrayList<UserInterest> user_interest;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public ArrayList<UserInterest> getUserInterest() {
            return user_interest;
        }

        public void setUserInterest(ArrayList<UserInterest> user_interest) {
            this.user_interest = user_interest;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getFacebookUserId() {
            return facebook_user_id;
        }

        public void setFacebookUserId(String facebook_user_id) {
            this.facebook_user_id = facebook_user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMsisdn() {
            return msisdn;
        }

        public void setMsisdn(String msisdn) {
            this.msisdn = msisdn;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return first_name;
        }

        public void setFirstName(String first_name) {
            this.first_name = first_name;
        }

        public String getLastName() {
            return last_name;
        }

        public void setLastName(String last_name) {
            this.last_name = last_name;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

    public DataObject getData() {
        return data;
    }

    public void setData(DataObject data) {
        this.data = data;
    }
}