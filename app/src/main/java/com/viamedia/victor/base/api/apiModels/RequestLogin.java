package com.viamedia.victor.base.api.apiModels;

public class RequestLogin {
    private String msisdn;
    private String password;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
