package com.viamedia.victor.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.ValidationManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordOtpActivity extends AppCompatActivity {
    private static final String TAG = "ForgotPwOtpActivity";
    private Context context;
    private EditText otp;
    private Button resetOtpButton, resendOtpButton;
    private WebService webService;
    private ValidationManager validationManager;
    private String msisdnWithoutPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_otp);
        context = this;

        if (getIntent().hasExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS)) {
            msisdnWithoutPlus = getIntent().getStringExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS);
            initManagers();
            initUi();

            if (getIntent().hasExtra(Constants.EXTRA_RESET_PASSWORD_OTP)) {
                otp.setText(getIntent().getStringExtra(Constants.EXTRA_RESET_PASSWORD_OTP));
            }
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, ForgotPasswordActivity.class));
        super.onBackPressed();
    }

    private void initUi() {
        otp = (EditText) findViewById(R.id.otp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // reset otp button
        resetOtpButton = (Button) findViewById(R.id.reset_button);
        resetOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        // resend otp button
        resendOtpButton = (Button) findViewById(R.id.resend_otp_button);
        resendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiRequestPasswordReset(msisdnWithoutPlus);
            }
        });
    }

    private void initManagers() {
        webService = MyApp.getInstance().getWebService();
        validationManager = MyApp.getInstance().getValidationManager();
    }

    public void validate() {
        if (validationManager.isForgotPasswordOtpFormValid(findViewById(android.R.id.content), otp)) {
            Intent intent = new Intent(context, ChangePasswordActivity.class);
            intent.putExtra(Constants.EXTRA_RESET_PASSWORD_OTP, String.valueOf(otp.getText()));
            intent.putExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS, msisdnWithoutPlus);
            startActivity(intent);
            finish();
        }
    }

    private void apiRequestPasswordReset(final String msisdn) {
        showLoading();

        Call<ResponseBase> call = webService.requestPasswordReset(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                getResources().getString(R.string.api_version_string),
                msisdn
        );
        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response != null && response.body() != null && response.body().isSuccess()) {
                    Snackbar.make(findViewById(android.R.id.content), R.string.reset_instructions_has_been_sent, Snackbar.LENGTH_LONG).show();
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    t.start();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.could_not_reset_password_please_try_again, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.could_not_reset_password_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }
}