package com.viamedia.victor.base.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.models.Comment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    private static final String TAG = "CommentsAdapter";
    private ArrayList<Comment> data;
    private AdapterInterface adapterInterface;
    private Context context;

    public CommentsAdapter(Context context, AdapterInterface adapterInterface) {
        this.context = context;
        this.adapterInterface = adapterInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView, divider;
        public TextView author, content, time;
        public ImageView image;

        public ViewHolder(final View itemView, final AdapterInterface adapterInterface) {
            super(itemView);
            mView = itemView;

            // ui
            author = (TextView) itemView.findViewById(R.id.item_author);
            time = (TextView) itemView.findViewById(R.id.item_time);
            content = (TextView) itemView.findViewById(R.id.item_content);
            image = (ImageView) itemView.findViewById(R.id.item_image);
            divider = (View) itemView.findViewById(R.id.item_divider);

            // click activate or deactivate item
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterInterface.interfaceResponse(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardview = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, parent, false);
        return new ViewHolder(cardview, adapterInterface);
    }

    @Override
    public void onBindViewHolder(final CommentsAdapter.ViewHolder holder, int position) {
        // default view
        if (data != null && data.get(position) != null) {
            if (data.get(position).getAuthor() != null) {
                if (data.get(position).getAuthor().getName() != null) {
                    holder.author.setText(data.get(position).getAuthor().getName());
                }
            } else {
                holder.author.setText(R.string.no_author_set);
            }

            if (position == data.size() - 1) {
                // last item in list
                holder.divider.setVisibility(View.GONE);
            } else {
                holder.divider.setVisibility(View.VISIBLE);
            }

            Date timeAsDate = data.get(position).getCreatedAtAsDate();
            if (timeAsDate != null) {
                holder.time.setText(MyApp.getInstance().getUtil().getNiceTime(timeAsDate));
                holder.time.setVisibility(View.VISIBLE);
            } else {
                holder.time.setVisibility(View.GONE);
            }
            holder.content.setText(data.get(position).getContent());

            // load image from url
            Glide.clear(holder.image);
            Glide.with(context)
                    .load(data.get(position).getAuthor().getAvatar())
                    .crossFade()
                    .error(R.drawable.flavoured_category_banner) // fallback image
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public interface AdapterInterface {
        void interfaceResponse(int position);
    }

    public void setData(ArrayList<Comment> data) {
        // latest comment show at the top
        Collections.reverse(data);
        this.data = data;
    }
}
