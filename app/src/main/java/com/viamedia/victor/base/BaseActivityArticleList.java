package com.viamedia.victor.base;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.viamedia.victor.base.adapters.ArticleListAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.PaymentOption;
import com.viamedia.victor.base.api.apiModels.PricingStrategy;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.customDialogs.CustomDialogPaidContentSignIn;
import com.viamedia.victor.base.customDialogs.CustomDialogPaymentSelection;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.ArticleLocal;
import com.viamedia.victor.base.util.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseActivityArticleList extends AppCompatActivity {
    private static final String TAG = "BaseActivityArticleList";
    private CustomDialogPaymentSelection customDialogPaymentSelection;
    private CustomDialogPaidContentSignIn customDialogPaidContentSignIn;

    public void showLoading() {
        showLoadingCore(this);
    }

    private void showLoadingCore(final BaseActivityArticleList activity) {
        if (activity != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LinearLayout loadingLayout = (LinearLayout) activity.findViewById(R.id.loading);
                    if (loadingLayout != null) {
                        loadingLayout.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    public void hideLoading() {
        hideLoadingCore(this);
    }

    private void hideLoadingCore(final BaseActivityArticleList activity) {
        if (activity != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LinearLayout loadingLayout = (LinearLayout) activity.findViewById(R.id.loading);
                    if (loadingLayout != null) {
                        loadingLayout.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void apiCreditsCheck(
            final ArrayList<ArticleLocal> data,
            final int position,
            final String contentSelection,
            String articleId,
            WebService webService,
            final StorageManager storageManager,
            final Util util,
            final Activity activity,
            final ArticleListAdapter adapter) {

        showLoading();
        Call<ResponseBase> call = webService.checkAndDeductCreditsForArticle(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager.getUserObject().getLoginToken(),
                getResources().getString(R.string.api_version_string),
                contentSelection,
                articleId);

        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");

                if (response.isSuccessful()) {
                    // has enough credits, open article
                    openArticleDetailedView(data, position, activity);
                } else {
                    dialogSelection(
                            data.get(position).getPricingStrategy(),
                            data.get(position).getPaymentOptions(),
                            storageManager,
                            util,
                            contentSelection,
                            adapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                hideLoading();
            }
        });
    }

    private void openArticleDetailedView(ArrayList<ArticleLocal> data, int position, Activity activity) {
        hideLoading();

        Intent intent = new Intent(activity, ArticleActivity.class);
        intent.putExtra(Constants.EXTRA_ARTICLE_ID, data.get(position).getId());
        intent.putExtra(Constants.EXTRA_ARTICLE_TITLE, data.get(position).getTitle());
        intent.putExtra(Constants.EXTRA_ARTICLE_HTML_CONTENT, data.get(position).getContent());
        intent.putExtra(Constants.EXTRA_ARTICLE_IMAGE_URL, data.get(position).getImageUrl());
        intent.putExtra(Constants.EXTRA_ARTICLE_URL, data.get(position).getUrl());
        // TODO: read server comments option
        intent.putExtra(Constants.EXTRA_ARTICLE_COMMENTS_ENABLED, false);
        startActivity(intent);
    }

    public void dialogSelection(PricingStrategy pricingStrategy,
                                ArrayList<PaymentOption> paymentOptions,
                                StorageManager storageManager,
                                Util util,
                                String contentSelection, ArticleListAdapter adapter) {
        // my profile(custom)
        if (storageManager.getUserObject() != null &&
                storageManager.getUserObject().getLoginToken() != null &&
                storageManager.getUserObject().getFacebookId() == null) {


            PaymentOption defaultPaymentOption = null;
            for (PaymentOption paymentOption : paymentOptions) {
                if (util.isDefaultPaymentOption(paymentOption)) {
                    defaultPaymentOption = paymentOption;
                }
            }

            final PaymentOption finalDefaultPaymentOption = defaultPaymentOption;
            if (defaultPaymentOption != null) {
                customDialogPaymentSelection = new CustomDialogPaymentSelection(
                        this,
                        pricingStrategy,
                        new ArrayList<PaymentOption>() {{
                            add(finalDefaultPaymentOption);
                        }},
                        storageManager,
                        contentSelection,
                        adapter);
                customDialogPaymentSelection.show();
            } else {
                // no default option set by server
            }

            // always notify user on fallback
            Snackbar.make(
                    findViewById(android.R.id.content),
                    R.string.you_dont_have_enough_credits_to_view_this_item,
                    Snackbar.LENGTH_LONG).show();
        }
        // my profile(facebook)
        else if (storageManager.getUserObject() != null && storageManager.getUserObject().getFacebookId() != null) {
            customDialogPaymentSelection = new CustomDialogPaymentSelection(
                    this,
                    pricingStrategy,
                    paymentOptions,
                    storageManager,
                    contentSelection, adapter);
            customDialogPaymentSelection.show();
        }
        // not logged in
        else {
            customDialogPaidContentSignIn = new CustomDialogPaidContentSignIn(this);
            customDialogPaidContentSignIn.show();
        }
    }

    public void dialogPayment(PricingStrategy pricingStrategy,
                              ArrayList<PaymentOption> paymentOptions,
                              StorageManager storageManager,
                              String contentSelection,
                              ArticleListAdapter adapter) {

        // my profile(custom)
        if (storageManager.getUserObject() != null &&
                storageManager.getUserObject().getLoginToken() != null &&
                storageManager.getUserObject().getFacebookId() == null) {

            customDialogPaymentSelection = new CustomDialogPaymentSelection(
                    this,
                    pricingStrategy,
                    paymentOptions,
                    storageManager,
                    contentSelection,
                    adapter);
            customDialogPaymentSelection.show();
        }
        // my profile(facebook)
        else if (storageManager.getUserObject() != null && storageManager.getUserObject().getFacebookId() != null) {
            customDialogPaymentSelection = new CustomDialogPaymentSelection(
                    this,
                    pricingStrategy,
                    paymentOptions,
                    storageManager,
                    contentSelection,
                    adapter);
            customDialogPaymentSelection.show();
        }
        // not logged in
        else {
            customDialogPaidContentSignIn = new CustomDialogPaidContentSignIn(this);
            customDialogPaidContentSignIn.show();
        }
    }

    public void handleIntefaceResponse(
            int position,
            StorageManager storageManager,
            Util util,
            WebService webService,
            Activity activity,
            ArticleListAdapter adapter) {

        showLoading();
        ArrayList<ArticleLocal> data = adapter.getData();

        if (data.get(position).getPricingStrategy() != null) {
            if (data.get(position).getPricingStrategy().getPrice() > 0) {
                // has price set
                if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_UNPAID)) {
                    // has not paid for this item
                    hideLoading();
                    dialogPayment(
                            data.get(position).getPricingStrategy(),
                            data.get(position).getPaymentOptions(),
                            storageManager,
                            data.get(position).getContentSelectionSlug(),
                            adapter);
                } else if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_PENDING)) {
                    // has paid, in pending state, waiting for 3rd party
                    hideLoading();
                    Snackbar.make(
                            findViewById(android.R.id.content),
                            R.string.payment_is_pending_please_try_again_later,
                            Snackbar.LENGTH_LONG).show();
                } else if (data.get(position).getPricingStrategy().getStatus().equals(Constants.PRICING_STATUS_PAID) &&
                        !data.get(position).getPricingStrategy().isCredits()) {
                    // not enough credits, fallback to ad-hoc payment to top up
                    hideLoading();
                    dialogSelection(
                            data.get(position).getPricingStrategy(),
                            data.get(position).getPaymentOptions(),
                            storageManager,
                            util,
                            data.get(position).getContentSelectionSlug(),
                            adapter);

                } else {
                    apiCreditsCheck(
                            data,
                            position,
                            data.get(position).getContentSelectionSlug(),
                            data.get(position).getId(),
                            webService,
                            storageManager,
                            util,
                            activity,
                            adapter);
                }
            } else {
                // no price, free item
                openArticleDetailedView(data, position, activity);
            }
        } else {
            // no pricing strategy was set
            openArticleDetailedView(data, position, activity);
        }
    }
}