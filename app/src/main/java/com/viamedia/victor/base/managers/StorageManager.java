package com.viamedia.victor.base.managers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.viamedia.victor.base.api.apiModels.GpsServiceObject;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;
import com.viamedia.victor.base.models.InterestsLocal;
import com.viamedia.victor.base.models.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;

public class StorageManager {
    private static final String TAG = "StorageManager";
    private Context context;
    private String path;
    private boolean useExternalStorage = false;
    private boolean useInternalStorage = false;

    public StorageManager(Context context) {
        this.context = context;

        // set path (try external 1st)
        this.path = Environment.getExternalStorageDirectory() + "/Android/data/" + context.getPackageName();
        File dir_external = new File(this.path);

        if (!dir_external.exists()) {
            if (!dir_external.mkdirs()) {

                // set path (try internal 2nd)
                // cannot create dir on external storage, try internal storage
                // /data/data/<package_name>/files
                this.path = String.valueOf(context.getFilesDir());
                File dir_internal = new File(this.path);

                if (!dir_internal.exists()) {
                    if (!dir_internal.mkdirs()) {
                        Log.e(TAG, "Could not create file on external storage or internal storage");
                    } else {
                        this.useInternalStorage = true;
                    }
                } else {
                    this.useInternalStorage = true;
                }

            } else {
                this.useExternalStorage = true;
            }
        } else {
            this.useExternalStorage = true;
        }
    }

    private String getPathOutsidePackage() {
        // this path is outside of the app namespace package, files will not be removed if app is removed
        return this.path + "/..";
    }

    private String getPath() {
        return this.path;
    }

    public String getUserFilename() {
        return getPath() + "/" + "user.json";
    }

    public String getInterestsFilename() {
        return getPath() + "/" + "interests.json";
    }

    public String getStorefrontFilename() {
        return getPath() + "/" + "storefrontResponse.json";
    }

    public String getGpsServiceObjectFilename() {
        return getPath() + "/" + "gpsServiceObject.json";
    }

    public String getContentSelectionFilename(String contentSelection) {
        return getPath() + "/" + "contentSelectionResponse_" + contentSelection.toLowerCase(Locale.getDefault()) + ".json";
    }

    public String getSearchResultFilename() {
        return getPath() + "/" + "searchResult.json";
    }

    public String getCommentFilename(String articleId) {
        return getPath() + "/" + String.format("comments_for_article_%1$s.json");
    }

    public User getUserObject() {
        File file = new File(getUserFilename());

        // read object from file
        if (file.exists()) {
            try {
                //convert the json string back to object
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                return (new Gson()).fromJson(bufferedReader, User.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public InterestsLocal getInterestsObject() {
        File file = new File(getInterestsFilename());

        // read object from file
        if (file.exists()) {
            try {
                //convert the json string back to object
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                return (new Gson()).fromJson(bufferedReader, InterestsLocal.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public ResponseBaseStoreFront getStoreFrontObject() {
        File file = new File(getStorefrontFilename());

        // read object from file
        if (file.exists()) {
            try {
                //convert the json string back to object
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                return (new Gson()).fromJson(bufferedReader, ResponseBaseStoreFront.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public GpsServiceObject getGpsServiceObject() {
        File file = new File(getGpsServiceObjectFilename());

        // read object from file
        if (file.exists()) {
            try {
                //convert the json string back to object
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                return (new Gson()).fromJson(bufferedReader, GpsServiceObject.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public ResponseBaseContentSelectionOrSection getContentSelectionObject(String contentSelection) {
        File file = new File(getContentSelectionFilename(contentSelection));

        // read object from file
        if (file.exists()) {
            try {
                //convert the json string back to object
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()));
                return (new Gson()).fromJson(bufferedReader, ResponseBaseContentSelectionOrSection.class);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return null;
    }

    public void saveDataToFile(File newFile, String data) {
        // create dir and or file
        if (!newFile.exists()) {
            File dir = newFile.getParentFile();
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    Log.e(TAG, "parent directory " + dir.getPath() + " created");
                }
                if (!dir.isDirectory()) {
                    throw new IllegalStateException("Unable to create directory " + dir.getPath());
                }
            }
        } else {
            if (newFile.isDirectory()) {
                throw new IllegalStateException("Save file can not be a folder");
            }
        }

        // saving data to file
        byte[] bytes;
        bytes = data.getBytes();

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            fileOutputStream.write(bytes);
            fileOutputStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        // keep this at the bottom, changes file property to directory
        Log.i(TAG, "saveDataToFile() " + newFile.getAbsolutePath());
    }
}
