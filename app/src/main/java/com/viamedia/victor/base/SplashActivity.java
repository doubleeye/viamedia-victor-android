package com.viamedia.victor.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.util.TelephonyUtils;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    UserManager userManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        userManager = new UserManager(this);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String country = TelephonyUtils.getUserCountry(MyApp.getInstance());
                    if (country.length() > 0) {
                        userManager.setCountry(country.toUpperCase(Locale.getDefault()));
                    }

                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                startActivity(new Intent(getApplication(), MainActivity.class));
                finish();
            }
        });
        t.start();
    }
}