package com.viamedia.victor.base.managers;

import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;

public class ValidationManager {
    private static final String TAG = "ValidationManager";

    public String isPhoneNumberValid(String s) {
        int countryCode = MyApp.getInstance().getUserManager().getCountryDialCode();

        // trim international format
        String nationalNumber = String.valueOf(s);
        if (nationalNumber.contains("+")) {
            nationalNumber = nationalNumber.replace("+", "");

            if (nationalNumber.indexOf(String.valueOf(countryCode)) == 0) {
                nationalNumber = nationalNumber.replace(String.valueOf(countryCode), "");
            }
        }

        // create new object
        Phonenumber.PhoneNumber ph = new Phonenumber.PhoneNumber();
        ph.setCountryCode(countryCode);
        try {
            long nationalNumberLong = Long.parseLong(nationalNumber);
            ph.setNationalNumber(nationalNumberLong);
        } catch (NumberFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // format number, require non-international format 0712223333
        if (PhoneNumberUtil.getInstance().isValidNumber(ph)) {
            return PhoneNumberUtil.getInstance().format(ph, PhoneNumberUtil.PhoneNumberFormat.E164);
        }
        return null;
    }

    public boolean isEmailValid(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isForgotPasswordOtpFormValid(
            View contentView,
            EditText otp) {

        if (otp.getText().length() < 2) {
            // otp
            Snackbar.make(contentView, R.string.error_invalid_otp, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean isFacebookLoginDialogFormValid(
            View contentView,
            EditText age,
            EditText city,
            EditText country) {

        if (age.getText().length() == 0 || String.valueOf(age.getText()).equalsIgnoreCase("0")) {
            // age
            snackbarCenterDialog(Snackbar.make(contentView, R.string.error_invalid_age, Snackbar.LENGTH_LONG)).show();
            return false;
        } else if (city.getText().length() < 1) {
            // submitValidation city
            snackbarCenterDialog(Snackbar.make(contentView, R.string.invalid_city_please_try_again, Snackbar.LENGTH_LONG)).show();
            return false;
        } else if (country.getText().length() < 1) {
            // submitValidation country
            snackbarCenterDialog(Snackbar.make(contentView, R.string.invalid_county_please_try_again, Snackbar.LENGTH_LONG)).show();
            return false;
        }
        return true;
    }

    public boolean isRegistrationFormValid(
            View contentView,
            EditText firstname,
            EditText surname,
            EditText password,
            EditText passwordConfirmation,
            EditText age,
            EditText email,
            EditText city,
            EditText country) {

        if (firstname.getText().length() == 0) {
            // name
            Snackbar.make(contentView, R.string.error_invalid_first_name, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (surname.getText().length() == 0) {
            // surname
            Snackbar.make(contentView, R.string.error_invalid_surname, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (password.getText().length() <= 5) {
            // password
            Snackbar.make(contentView, R.string.error_invalid_password, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (passwordConfirmation.getText().length() <= 5) {
            // password confirmation
            Snackbar.make(contentView, R.string.error_invalid_password_confirm, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!String.valueOf(passwordConfirmation.getText()).equals(String.valueOf(password.getText()))) {
            // password confirmation match password
            Snackbar.make(contentView, R.string.error_invalid_password_confirm_does_not_match, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (age.getText().length() == 0 || String.valueOf(age.getText()).equalsIgnoreCase("0")) {
            // age
            Snackbar.make(contentView, R.string.error_invalid_age, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (email.getText().length() > 0 && !isEmailValid(email.getText())) {
            // (optional) email validation
            Snackbar.make(contentView, R.string.error_invalid_email, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (city.getText().length() == 0) {
            // city
            Snackbar.make(contentView, R.string.error_invalid_city, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (country.getText().length() == 0) {
            // country
            Snackbar.make(contentView, R.string.error_invalid_country, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean isLoginFormValid(
            View contentView,
            EditText txtPass) {

        if (txtPass.getText().length() < 6) {
            // Password
            Snackbar.make(contentView, R.string.error_invalid_password, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean isEditMyProfileCustomFormValid(
            View contentView,
            EditText firstname,
            EditText surname,
            EditText age,
            EditText email,
            EditText city,
            EditText country) {

        if (firstname.getText().length() == 0) {
            // name
            Snackbar.make(contentView, R.string.error_invalid_first_name, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (surname.getText().length() == 0) {
            // surname
            Snackbar.make(contentView, R.string.error_invalid_surname, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (age.getText().length() == 0) {
            // age
            Snackbar.make(contentView, R.string.error_invalid_age, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (email.getText().length() > 0 && !isEmailValid(email.getText())) {
            // (optional) email validation
            Snackbar.make(contentView, R.string.error_invalid_email, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (city.getText().length() == 0) {
            // city
            Snackbar.make(contentView, R.string.error_invalid_city, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (country.getText().length() == 0) {
            // country
            Snackbar.make(contentView, R.string.error_invalid_country, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean isChangePasswordFormValid(
            View contentView,
            EditText password,
            EditText passwordConfirmation) {

        if (password.getText().length() <= 5) {
            // password
            Snackbar.make(contentView, R.string.error_invalid_password, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (passwordConfirmation.getText().length() <= 5) {
            // password confirmation
            Snackbar.make(contentView, R.string.error_invalid_password_confirm, Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!String.valueOf(passwordConfirmation.getText()).equals(String.valueOf(password.getText()))) {
            // password confirmation match password
            Snackbar.make(contentView, R.string.error_invalid_password_confirm_does_not_match, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private Snackbar snackbarCenterDialog(Snackbar snackbar) {
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        view.setLayoutParams(params);
        return snackbar;
    }
}
