package com.viamedia.victor.base.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import com.viamedia.victor.base.R;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.models.User;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImageUtil {
    private static final String TAG = "ImageUtil";

    // encode and decode image to and from base64 string
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        int quality = Constants.IMAGE_UPLOAD_QUALITY;
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
    }

    public static Bitmap decodeBase64(String input) {
        if (input != null) {
            byte[] decodedBytes = Base64.decode(input, 0);
            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        }
        return null;
    }

    public Bitmap rotateBitmap(File f, Bitmap bitmap) {
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(f.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight(),
                matrix,
                true);
    }

    public void onActivityResultCameraGallery(
            int resultCode,
            int requestCode,
            Intent intent,
            Activity activity,
            DynamicImageView profileImage,
            User user) {
        Bitmap bitmap = null;
        String selectedImagePath = null;

        if (resultCode == activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_SELECT_PHOTO_GALLERY) {
            if (intent.getData() != null) {

                Uri selectedImageUri = intent.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};

                Cursor c = activity.getContentResolver().query(selectedImageUri, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                selectedImagePath = c.getString(columnIndex);
                c.close();

                bitmap = BitmapFactory.decodeFile(selectedImagePath); // load
                if (bitmap != null) {
                    bitmap = resizeKeepAspect(bitmap,
                            activity.getResources().getInteger(R.integer.bitmap_sample_width),
                            activity.getResources().getInteger(R.integer.bitmap_sample_height));

                    bitmap = rotateBitmap(new File(selectedImagePath), bitmap);
                    // save bitmap to device
                    user.setAvatarBitmap(this, bitmap);
                    // show on ui
                    profileImage.setImageBitmap(bitmap);
                } else {
                    Snackbar.make(activity.findViewById(android.R.id.content), R.string.could_not_save_the_image, Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(activity.findViewById(android.R.id.content), R.string.cancelled, Snackbar.LENGTH_LONG).show();
            }
        } else if (resultCode == activity.RESULT_OK && requestCode == Constants.REQUEST_CODE_SELECT_PHOTO_CAMERA) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            File[] fileList = f.listFiles();
            if (fileList != null) {
                for (File temp : fileList) {
                    if (temp.getName().equals(activity.getString(R.string.temp_jpg))) {
                        f = temp;
                        break;
                    }
                }
            } else {
                f = new File(f.getAbsolutePath() + "/" + activity.getString(R.string.temp_jpg));
            }

            if (!f.exists()) {
                Snackbar.make(activity.findViewById(android.R.id.content), R.string.error_while_capturing_image, Snackbar.LENGTH_LONG).show();
                return;
            }

            if (f.isFile()) {
                try {
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                    if (bitmap != null) {
                        bitmap = resizeKeepAspect(bitmap,
                                activity.getResources().getInteger(R.integer.bitmap_sample_width),
                                activity.getResources().getInteger(R.integer.bitmap_sample_height));

                        bitmap = rotateBitmap(f, bitmap);

                        // save bitmap to device
                        user.setAvatarBitmap(this, bitmap);
                        // show on ui
                        profileImage.setImageBitmap(bitmap);
                    } else {
                        Snackbar.make(activity.findViewById(android.R.id.content), R.string.could_not_save_the_image, Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, activity.getResources().getString(R.string.could_not_save_the_image));
                }
            }
        }
    }

    private static Bitmap resizeKeepAspect(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
        }
        return image;
    }

    public AlertDialog.Builder showChooserDialogCameraGallery(final Activity activity) {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(activity);
        myAlertDialog.setTitle(activity.getString(R.string.select_profile_picture));
        myAlertDialog.setMessage(activity.getString(R.string.how_do_you_want_to_set_your_picture));

        myAlertDialog.setPositiveButton(activity.getString(R.string.gallery),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pictureActionIntent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        // show system gallery
                        activity.startActivityForResult(
                                pictureActionIntent,
                                Constants.REQUEST_CODE_SELECT_PHOTO_GALLERY);
                    }
                });

        myAlertDialog.setNegativeButton(activity.getString(R.string.camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment
                                .getExternalStorageDirectory(), activity.getString(R.string.temp_jpg));
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(f));

                        // show system camera
                        activity.startActivityForResult(intent,
                                Constants.REQUEST_CODE_SELECT_PHOTO_CAMERA);
                    }
                });
        return myAlertDialog;
    }
}
