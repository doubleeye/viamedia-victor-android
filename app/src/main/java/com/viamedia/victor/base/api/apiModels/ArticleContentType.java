package com.viamedia.victor.base.api.apiModels;

/**
 * Created by marais on 2016/05/20.
 */
public class ArticleContentType {
    private int id;
    private String slug;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
