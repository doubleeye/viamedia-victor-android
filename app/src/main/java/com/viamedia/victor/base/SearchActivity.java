package com.viamedia.victor.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.viamedia.victor.base.adapters.ArticleListAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.ResponseBaseSearch;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.util.Util;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivityArticleList {
    private static final String TAG = "SearchActivity";
    private Context context;
    private RecyclerView recyclerView;
    private String query;
    private SearchView searchView;
    private WebService webService;
    private StorageManager storageManager;
    private Util util;
    private TextView recyclerEmptyText;
    private Activity activity;
    private ArticleListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = this;

        // activity requires Constants.EXTRA_SEARCH_STRING
        if (getIntent().hasExtra(Constants.EXTRA_SEARCH_STRING)) {
            query = getIntent().getStringExtra(Constants.EXTRA_SEARCH_STRING);

            initManagers();
            initUi();
            apiSearch(query);
        } else {
            finish();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    private void initManagers() {
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
        util = MyApp.getInstance().getUtil();
    }

    private void initUi() {
        activity = this;
        recyclerEmptyText = (TextView) findViewById(R.id.recycler_view_empty_text_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.search);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // adapter
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.requestFocus();
    }

    private void initAdapterWithData(final ResponseBaseSearch result) {
        if (result != null && result.getData() != null && recyclerView != null) {
            adapter = new ArticleListAdapter(this, null, result, 2, new ArticleListAdapter.AdapterInterface() {
                @Override
                public void interfaceResponse(int position) {
                    handleIntefaceResponse(
                            position,
                            storageManager,
                            util,
                            webService,
                            activity,
                            adapter);
                }
            });
            recyclerView.setAdapter(adapter);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void apiSearch(final String query) {
        hideKeyboard();
        showLoading();
        if (recyclerEmptyText != null) {
            recyclerEmptyText.setVisibility(View.GONE);
        }

        Call<ResponseBaseSearch> call = webService.getSearch(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                String.format(getString(R.string.items_range), 0, getResources().getInteger(R.integer.max_result_search_feed)),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                query
        );
        call.enqueue(new Callback<ResponseBaseSearch>() {
            @Override
            public void onResponse(Call<ResponseBaseSearch> call, final Response<ResponseBaseSearch> response) {
                Log.i(TAG, "onSuccess()");
                if (response.body() != null &&
                        response.body().isSuccess() &&
                        response.body().getData() != null) {


                    // save for offline use
                    storageManager.saveDataToFile(
                            new File(storageManager.getSearchResultFilename()),
                            (new Gson()).toJson(response.body())
                    );

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.body().getData().size() > 0) {
                                recyclerEmptyText.setVisibility(View.GONE);
                            } else {
                                recyclerEmptyText.setVisibility(View.VISIBLE);
                            }
                            initAdapterWithData(response.body());
                        }
                    });

                    hideLoading();
                } else {
                    Log.e(TAG, "Failed to retrieve data");
                    Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerEmptyText.setVisibility(View.VISIBLE);
                        }
                    });
                    hideLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseSearch> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerEmptyText.setVisibility(View.VISIBLE);
                    }
                });
                hideLoading();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_search, menu);
        // set icon drawable
        menu.findItem(R.id.action_search).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_search));

        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        if (null != searchView) {
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                public boolean onQueryTextChange(String newText) {
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    SearchActivity.this.query = query;
                    searchView.setQuery("", false);
                    searchView.setIconified(true);

                    apiSearch(SearchActivity.this.query);
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }

        return true;
    }
}