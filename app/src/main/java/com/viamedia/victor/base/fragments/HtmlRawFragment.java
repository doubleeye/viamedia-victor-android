package com.viamedia.victor.base.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.viamedia.victor.base.R;
import com.viamedia.victor.base.constants.Constants;


public class HtmlRawFragment extends Fragment {
    private static final String ARG_HTML_CONTENT = "HtmlRawFragment";

    public static HtmlRawFragment newInstance(String htmlContent) {
        HtmlRawFragment fragment = new HtmlRawFragment();
        Bundle args = new Bundle();

        args.putString(ARG_HTML_CONTENT, htmlContent);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_html_raw, container, false);
        WebView webView = (WebView) rootView;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);

        if (getArguments().get(ARG_HTML_CONTENT) != null) {
            webView.loadDataWithBaseURL(
                    "file:///android_asset/webview",
                    String.valueOf(getArguments().get(ARG_HTML_CONTENT)),
                    "text/html",
                    Constants.ENCODING_HTML,
                    null
            );
        } else {
            webView.loadUrl("file:///android_asset/html/local.html");
        }

        return rootView;
    }
}