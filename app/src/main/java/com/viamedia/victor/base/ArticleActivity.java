package com.viamedia.victor.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.viamedia.victor.base.adapters.CommentsAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestAddComment;
import com.viamedia.victor.base.api.apiModels.ResponseBaseArticleComments;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.fragments.HtmlRawFragment;
import com.viamedia.victor.base.managers.StorageManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleActivity extends AppCompatActivity {
    private static final String TAG = "ArticleActivity";
    private String id, title, htmlContent, imageUrl, articleUrl;
    private Context context;
    private ImageView articleImage;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private TextView navTitle, navDescription;
    private ImageView navImage;
    private boolean commentsEnabled;
    private EditText commentText;
    private Button commentButton;
    private RecyclerView recyclerView;
    private CommentsAdapter adapter;
    private WebService webService;
    private StorageManager storageManager;
    private ActionBarDrawerToggle toggle;
    private TextView recyclerEmptyText;
    private DrawerLayout drawer;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        if (getIntent().hasExtra(Constants.EXTRA_ARTICLE_TITLE) &&
                getIntent().hasExtra(Constants.EXTRA_ARTICLE_HTML_CONTENT) &&
                getIntent().hasExtra(Constants.EXTRA_ARTICLE_IMAGE_URL) &&
                getIntent().hasExtra(Constants.EXTRA_ARTICLE_ID) &&
                getIntent().hasExtra(Constants.EXTRA_ARTICLE_COMMENTS_ENABLED)) {

            title = getIntent().getStringExtra(Constants.EXTRA_ARTICLE_TITLE);
            htmlContent = getIntent().getStringExtra(Constants.EXTRA_ARTICLE_HTML_CONTENT);
            imageUrl = getIntent().getStringExtra(Constants.EXTRA_ARTICLE_IMAGE_URL);
            id = getIntent().getStringExtra(Constants.EXTRA_ARTICLE_ID);
            articleUrl = getIntent().getStringExtra(Constants.EXTRA_ARTICLE_URL);
            commentsEnabled = getIntent().getBooleanExtra(Constants.EXTRA_ARTICLE_COMMENTS_ENABLED, false);

            if (commentsEnabled) {
                setContentView(R.layout.activity_article_comments_enabled);
            } else {
                setContentView(R.layout.activity_article_comments_disabled);
            }

            initManagers();
            if (commentsEnabled) {
                setupNavDrawer();
            }
            initUi();
            loadData();
        } else {
            finish();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (commentsEnabled) {
            getMenuInflater().inflate(R.menu.activity_article_comments_enabled, menu);
        } else {
            getMenuInflater().inflate(R.menu.activity_article_comments_disabled, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_comment:
                toggleDrawer(drawer);
                return true;

            case R.id.action_share:
                shareIntent();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (commentsEnabled) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer != null && drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void initManagers() {
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void initUi() {
        articleImage = (ImageView) findViewById(R.id.toolbar_image);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerEmptyText = (TextView) findViewById(R.id.recycler_view_empty_text_view);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        initAdapter();
    }

    private void initAdapter() {
        // new adapter
        if (adapter == null) {
            adapter = new CommentsAdapter(context, new CommentsAdapter.AdapterInterface() {
                @Override
                public void interfaceResponse(int position) {
                    // TODO: reply onclick
                }
            });
        }
        recyclerView.setAdapter(adapter);
    }

    private void setupNavDrawer() {
        // drawer toggle
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // drawer item click
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navTitle = (TextView) navigationView.findViewById(R.id.nav_header_title);
        navDescription = (TextView) navigationView.findViewById(R.id.nav_header_description);
        navImage = (ImageView) navigationView.findViewById(R.id.nav_header_image);

        // comment ui
        LinearLayout commentLayout = (LinearLayout) navigationView.findViewById(R.id.new_comment_layout);
        commentText = (EditText) navigationView.findViewById(R.id.new_comment_text);
        commentButton = (Button) navigationView.findViewById(R.id.new_comment_button);

        if (storageManager.getUserObject() != null) {
            if (storageManager.getUserObject().getLoginToken() != null || storageManager.getUserObject().getFacebookId() != null) {
                // user logged in
                commentLayout.setVisibility(View.VISIBLE);
            } else {
                // user not logged in
                commentLayout.setVisibility(View.GONE);
            }
        } else {
            commentLayout.setVisibility(View.GONE);
        }

        // ui actions
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                commentText.requestFocus();
                commentText.performClick();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_IDLE && drawer.isDrawerOpen(GravityCompat.END)) {
                    commentText.requestFocus();
                }
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // dynamic drawer items onclick
                startActivity(item.getIntent());

                // close drawer
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentTextString = String.valueOf(commentText.getText());
                commentText.setText("");

                if (commentTextString.length() > 0) {
                    linearLayoutManager.scrollToPosition(0);
                    hideKeyboard();

                    if (storageManager.getUserObject() != null && storageManager.getUserObject().getLoginToken() != null) {
                        // user has to be logged in to be able to comment
                        // TODO: dynamic comment title
                        apiAddComment(
                                id,
                                "Comment title",
                                commentTextString,
                                storageManager.getUserObject().getLoginToken()
                        );
                    }
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.please_enter_a_valid_comment, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void toggleDrawer(DrawerLayout drawer) {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            try {
                drawer.openDrawer(GravityCompat.END);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void apiAddComment(final String articleId, String title, String content, String loginToken) {
        showLoading();
        final RequestAddComment requestAddComment = new RequestAddComment();
        requestAddComment.setTitle(title);
        requestAddComment.setContent(content);
        requestAddComment.setToken(loginToken);

        Call<ResponseBaseArticleComments> call = webService.addComment(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                articleId,
                requestAddComment
        );
        call.enqueue(new Callback<ResponseBaseArticleComments>() {
            @Override
            public void onResponse(Call<ResponseBaseArticleComments> call, final Response<ResponseBaseArticleComments> response) {
                Log.i(TAG, "onSuccess()");

                if (response.body() != null &&
                        response.body().isSuccess() &&
                        response.body().getData() != null) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (adapter != null) {
                                adapter.setData(response.body().getData());
                                adapter.notifyDataSetChanged();
                            }
                            recyclerEmptyText.setVisibility(View.GONE);
                        }
                    });
                }
                hideLoading();
            }

            @Override
            public void onFailure(Call<ResponseBaseArticleComments> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void shareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(getString(R.string.text_plain));
        intent.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.share_intent_text), getString(R.string.app_name), title, articleUrl));
        startActivity(Intent.createChooser(intent, getString(R.string.share_with)));
    }

    private void loadData() {
        // load toolbar image
        Glide
                .with(context)
                .load(imageUrl)
                .crossFade()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new articleImage once a day
                .into(articleImage);

        // load drawer header image
        if (commentsEnabled) {
            navTitle.setText(R.string.comments);
            navDescription.setText(title);

            Glide
                    .with(context)
                    .load(imageUrl)
                    .crossFade()
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new articleImage once a day
                    .into(navImage);
        }

        // load html content into fragment
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() == 0 ||
                getSupportFragmentManager().getFragments() == null) {

            htmlContent = replaceHtmlStringTokens(htmlContent);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.nested_scroll_view, HtmlRawFragment.newInstance(htmlContent), null)
                    .disallowAddToBackStack()
                    .commit();
        }

        // load comments
        apiComments(id);
    }

    private String replaceHtmlStringTokens(String str){
        str = str.replace("OSWALD.CSS", "/oswald.css");
        str = str.replace("CLIENT.CSS", "/client.css");
        str = str.replace("FONT-AWESOME.MIN.CSS", "/font-awesome.min.css");
        str = str.replace("JQUERY.MIN.JS", "/jquery.js");
        str = str.replace("MEDIAELEMENTPLAYER.MIN.CSS", "/mediaelementplayer.min.css");
        str = str.replace("MEDIAELEMENTPLAYER.MIN.JS", "/mediaelement-and-player.min.js");

        return str;
    }

    private void apiComments(final String articleId) {
        Call<ResponseBaseArticleComments> call = webService.getArticleComments(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                articleId
        );
        call.enqueue(new Callback<ResponseBaseArticleComments>() {
            @Override
            public void onResponse(Call<ResponseBaseArticleComments> call, final Response<ResponseBaseArticleComments> response) {
                Log.i(TAG, "onSuccess()");

                if (response.body() != null && response.body().getData() != null && response.body().isSuccess()) {
                    adapter.setData(response.body().getData());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.body().getData().size() > 0) {
                                recyclerEmptyText.setVisibility(View.GONE);
                            } else {
                                recyclerEmptyText.setVisibility(View.VISIBLE);
                            }

                            adapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerEmptyText.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseArticleComments> call, Throwable t) {
                Log.i(TAG, "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.please_check_your_internet_connection, Snackbar.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerEmptyText.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }
}