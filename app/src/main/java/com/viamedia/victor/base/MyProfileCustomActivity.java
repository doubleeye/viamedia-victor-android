package com.viamedia.victor.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

public class MyProfileCustomActivity extends AppCompatActivity {

    private static final String TAG = "MyProfileCustomActivity";
    private Context context;
    private StorageManager storageManager;
    private ImageUtil imageUtil;
    private Util util;
    private ImageView profileImage;
    private TextView firstName, lastname, age, gender, email, changePassword, city, country, mobileNumber;
    private Button interests;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_custom);
        context = this;

        initManagers();
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        imageUtil = MyApp.getInstance().getImageUtil();
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void initUi() {
        profileImage = (ImageView) findViewById(R.id.image_view);
        firstName = (TextView) findViewById(R.id.first_name);
        lastname = (TextView) findViewById(R.id.surname);
        age = (TextView) findViewById(R.id.age);
        gender = (TextView) findViewById(R.id.gender);
        email = (TextView) findViewById(R.id.email);
        city = (TextView) findViewById(R.id.city);
        country = (TextView) findViewById(R.id.country);
        mobileNumber = (TextView) findViewById(R.id.mobile_number);
        changePassword = (TextView) findViewById(R.id.change_password);
        interests = (Button) findViewById(R.id.interests_button);

        // only show email if available
        if (storageManager.getUserObject().getEmail() != null) {
            email.setVisibility(View.VISIBLE);
            email.setText(storageManager.getUserObject().getEmail());
        } else {
            email.setVisibility(View.GONE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // actions
        // change password
        SpannableString str = new SpannableString(changePassword.getText());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        changePassword.setText(str);
        changePassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Intent intent = new Intent(MyProfileCustomActivity.this, ChangePasswordActivity.class);
                    intent.putExtra(Constants.EXTRA_MSISDN_WITHOUT_PLUS, user.getMsisdnWithoutPlus());
                    intent.putExtra(Constants.EXTRA_RESET_PASSWORD_CUSTOM_PROFILE, true);
                    startActivity(intent);
                    v.performClick();
                }
                return true;
            }
        });

        interests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyProfileCustomActivity.this, MyInterestsActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_my_profile, menu);
        // set icon drawable
        menu.findItem(R.id.action_edit).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_edit));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                // show edit profile activity
                startActivity(new Intent(MyProfileCustomActivity.this, EditMyProfileCustomActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadData() {
        user = storageManager.getUserObject();

        // user data already received via login/registration
        if (user != null && user.getFirstname() != null) {
            firstName.setText(user.getFirstname());
        } else {
            firstName.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getLastname() != null) {
            lastname.setText(user.getLastname());
        } else {
            lastname.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getAge() != null) {
            age.setText(user.getAge());
        } else {
            // check if birthday is available and work out age
            age.setText(getString(R.string.unavailable));
            if (user != null && user.getBirthday() != null) {
                String ageString = util.getAgeFromFacebookBirthday(user.getBirthday());
                if (ageString != null) {
                    age.setText(String.valueOf(ageString));
                }
            }
        }
        if (user != null && user.getGender() != null) {
            gender.setText(user.getGenderTitleCase(util));
        } else {
            gender.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getEmail() != null) {
            email.setText(user.getEmail());
        } else {
            email.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getMsisdnWithPlus() != null) {
            mobileNumber.setText(user.getMsisdnWithPlus());
        } else {
            mobileNumber.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getCity() != null) {
            city.setText(user.getCity());
        } else {
            city.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getCity() != null) {
            city.setText(user.getCity());
        } else {
            city.setText(getString(R.string.unavailable));
        }
        if (user != null && user.getCountry() != null) {
            country.setText(user.getCountry());
        } else {
            country.setText(getString(R.string.unavailable));
        }

        // load profile image
        if (storageManager.getUserObject() != null) {
            // custom profile image
            if (storageManager.getUserObject().getAvatarBitmap(imageUtil) != null) {
                // load profile image from device
                profileImage.setImageBitmap(storageManager.getUserObject().getAvatarBitmap(imageUtil));
            } else if (storageManager.getUserObject().getAvatar() != null) {
                // load profile image from url
                Glide
                        .with(context)
                        .load(storageManager.getUserObject().getAvatar())
                        .crossFade()
                        .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new image once a day
                        .into(profileImage);
            }
        } else {
            // load fallback image when not logged in
            profileImage.setImageResource(R.drawable.flavoured_category_banner);
        }
    }
}