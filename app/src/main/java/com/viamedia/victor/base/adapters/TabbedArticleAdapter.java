package com.viamedia.victor.base.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;
import com.viamedia.victor.base.fragments.ArticleListFragmentType1;
import com.viamedia.victor.base.util.Util;

import java.util.ArrayList;

public class TabbedArticleAdapter extends FragmentPagerAdapter {
    private ArrayList<ResponseBaseStoreFront.DataObject.ResponseStoreFrontSection.ResponseStoreFrontSectionCategory> categories;
    private Util util;

    public TabbedArticleAdapter(
            ArrayList<ResponseBaseStoreFront.DataObject.ResponseStoreFrontSection.ResponseStoreFrontSectionCategory> categories,
            FragmentManager fm,
            Util util) {
        super(fm);

        this.util = util;
        this.categories = categories;
    }

    @Override
    public Fragment getItem(int position) {
        return ArticleListFragmentType1.newInstance(
                String.valueOf(getPageTitle(position)),
                util.getNameFromContentSelectionUrl(categories.get(position).getUrl()));
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).getName();
    }
}