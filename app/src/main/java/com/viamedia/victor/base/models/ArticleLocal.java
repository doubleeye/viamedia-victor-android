package com.viamedia.victor.base.models;

import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.api.apiModels.ArticleContentType;
import com.viamedia.victor.base.api.apiModels.PaymentOption;
import com.viamedia.victor.base.api.apiModels.PricingStrategy;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.api.apiModels.ResponseBaseSearch;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;

import java.util.ArrayList;
import java.util.Date;

public class ArticleLocal {
    private static final String TAG = "ArticleLocal";
    private String id;
    private String title;
    private String description;
    private String contentSelectionSlug;
    private String time;
    private String imageUrl;
    private String url;
    private String content;
    private PricingStrategy pricingStrategy;
    private ArrayList<PaymentOption> paymentOptions;
    private ArticleContentType contentType;

    public ArticleLocal(ResponseBaseContentSelectionOrSection.DataObject.ResponseContentSelectionResult.ResponseContentSelectionResultContentItem item, String contentSelectionSlug) {
        this.id = String.valueOf(item.getId());
        this.title = item.getTitle();
        this.description = item.getDescription();
        this.contentSelectionSlug = contentSelectionSlug;
        this.time = item.getModifiedAt();
        if (item.getFeaturedImage() != null) {
            this.imageUrl = item.getFeaturedImage().getMediumPath();
        }
        this.content = item.getContent();
        this.contentType = item.getContentTypes();
        this.url = item.getUrl();
        this.paymentOptions = item.getPaymentOptions();
        this.pricingStrategy = item.getPricingStrategy();
        this.paymentOptions = item.getPaymentOptions();
    }

    public ArticleLocal(ResponseBaseSearch.SearchItem item, String contentSelectionSlug) {
        this.id = String.valueOf(item.getId());
        this.title = item.getTitle();
        this.description = item.getDescription();
        this.contentSelectionSlug = contentSelectionSlug;
        this.time = item.getModifiedAt();
        if (item.getFeaturedImage() != null) {
            this.imageUrl = item.getFeaturedImage().getMediumPath();
        }
        this.content = item.getContent();
        this.contentType = item.getContentTypes();
        this.url = item.getUrl();
        this.paymentOptions = item.getPaymentOptions();
        this.pricingStrategy = getPricingStrategy();
        this.paymentOptions = item.getPaymentOptions();
    }

    public ArticleLocal(ResponseBaseStoreFront.DataObject.ResponseStoreFrontVoucher item) {
        this.title = item.getTitle();
        this.imageUrl = item.getImage();
        this.id = String.valueOf(item.getUuid());
        this.content = item.getContent();
    }


    public String getContentSelectionSlug() {
        return contentSelectionSlug;
    }

    public void setContentSelectionSlug(String contentSelectionSlug) {
        this.contentSelectionSlug = contentSelectionSlug;
    }

    public ArticleContentType getContentType() {
        return contentType;
    }

    public void setContentType(ArticleContentType contentTypes) {
        this.contentType = contentTypes;
    }

    public PricingStrategy getPricingStrategy() {
        return pricingStrategy;
    }

    public void setPricingStrategy(PricingStrategy pricingStrategy) {
        this.pricingStrategy = pricingStrategy;
    }

    public ArrayList<PaymentOption> getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(ArrayList<PaymentOption> paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTime() {
        return time;
    }

    public Date getTimeAsDate() {
        return MyApp.getInstance().getUtil().parseTime(this.time);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
