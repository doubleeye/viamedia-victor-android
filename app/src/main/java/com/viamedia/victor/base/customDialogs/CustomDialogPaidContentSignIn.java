package com.viamedia.victor.base.customDialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.viamedia.victor.base.LoginActivity;
import com.viamedia.victor.base.R;

public class CustomDialogPaidContentSignIn extends Dialog {
    public Activity activity;
    public Dialog dialog;
    public Button positiveButton, cancelButton;

    public CustomDialogPaidContentSignIn(Activity activity) {
        super(activity);
        this.activity = activity;
        this.dialog = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_paid_content_sign_in);

        positiveButton = (Button) findViewById(R.id.positive_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        // actions
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positiveButtonClick(v);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    private void positiveButtonClick(View v) {
        // show login screen
        getContext().startActivity(new Intent(activity, LoginActivity.class));
        activity.finish();
    }
}