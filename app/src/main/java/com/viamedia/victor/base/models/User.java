package com.viamedia.victor.base.models;

import android.graphics.Bitmap;

import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

public class User {
    private int id;
    private String loginToken;
    private String facebookLoginToken;
    private String facebookId;
    private String facebookProfilePicturePath;
    private String email;
    private String firstname;
    private String city;
    private String country;
    private String age;
    private String birthday;
    private String gender;
    private String lastname;
    private String msisdnWithoutPlus;
    // do not save user password

    public String getCountry() {
        return country;
    }

    private String avatar;

    public void setCountry(String country) {
        this.country = country;
    }

    public Bitmap getAvatarBitmap(ImageUtil imageUtil) {
        return imageUtil.decodeBase64(avatar);
    }

    public String getAvatarBase64String() {
        return avatar;
    }

    public void setAvatarBitmap(ImageUtil imageUtil, Bitmap avatar) {
        this.avatar = imageUtil.encodeToBase64(avatar, Bitmap.CompressFormat.JPEG);
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderTitleCase(Util util) {
        return util.toTitleCase(gender);
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getFacebookLoginToken() {
        return facebookLoginToken;
    }

    public void setFacebookLoginToken(String facebookLoginToken) {
        this.facebookLoginToken = facebookLoginToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMsisdnWithoutPlus() {
        return msisdnWithoutPlus;
    }

    public String getMsisdnWithPlus() {
        if (msisdnWithoutPlus != null) {
            return "+" + msisdnWithoutPlus;
        }
        return msisdnWithoutPlus;
    }

    public void setMsisdnWithoutPlus(String msisdnWithPlus) {
        if (msisdnWithPlus != null && msisdnWithPlus.length() >= 2) {
            // strip plus from string
            this.msisdnWithoutPlus = MyApp.getInstance().getUtil().removePlusFromNumber(msisdnWithPlus);
        } else {
            this.msisdnWithoutPlus = null;
        }
    }
}
