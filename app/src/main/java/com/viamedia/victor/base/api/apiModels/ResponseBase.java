package com.viamedia.victor.base.api.apiModels;


public class ResponseBase {
    private boolean success;
    private String response_code;
    private String api_version;
    private String message;
    private String base_url;
//    private ArrayList<String> errors;

//    public ArrayList<String> containedInErrors(String stringToFind) {
//        for (String error : errors) {
//            error.toLowerCase().contains(stringToFind.toLowerCase());
//        }
//        return errors;
//    }

//    public ArrayList<String> getErrors() {
//        return errors;
//    }

//    public String getErrorsAsString() {
//        String errors = null;
//        for (String error : getErrors()) {
//            if (errors == null) {
//                errors = error;
//            } else {
//                errors += "\n" + error;
//            }
//        }
//        return errors;
//    }

//    public void setErrors(ArrayList<String> errors) {
//        this.errors = errors;
//    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getResponseCode() {
        return response_code;
    }

    public void setResponseCode(String response_code) {
        this.response_code = response_code;
    }

    public String getApiVersion() {
        return api_version;
    }

    public void setApiVersion(String api_verison) {
        this.api_version = api_verison;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBaseUrl() {
        return base_url;
    }

    public void setBaseUrl(String base_url) {
        this.base_url = base_url;
    }
}