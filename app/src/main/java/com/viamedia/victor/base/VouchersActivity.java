package com.viamedia.victor.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viamedia.victor.base.adapters.VoucherListAdapter;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.ArticleLocal;

import java.util.ArrayList;

public class VouchersActivity extends AppCompatActivity {
    private static final String TAG = "VouchersActivity";
    private Context context;
    private RecyclerView recyclerView;
    private StorageManager storageManager;
    private TextView recyclerEmptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vouchers);
        context = this;

        initManagers();
        initUi();
        loadData();
    }

    private void initManagers() {
        storageManager = MyApp.getInstance().getStorageManager();
    }

    private void initUi() {
        recyclerEmptyText = (TextView) findViewById(R.id.recycler_view_empty_text_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.vouchers);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // adapater
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.requestFocus();
    }

    private void loadData() {
        initAdapterWithData(storageManager.getStoreFrontObject().getData().getVouchers());
    }

    private void initAdapterWithData(final ArrayList<ResponseBaseStoreFront.DataObject.ResponseStoreFrontVoucher> data) {
        final ArrayList dataUi = new ArrayList<>();
        for (final ResponseBaseStoreFront.DataObject.ResponseStoreFrontVoucher item : data) {
            dataUi.add(new ArticleLocal(item));
        }

        VoucherListAdapter adapter = new VoucherListAdapter(context, dataUi, new VoucherListAdapter.AdapterInterface() {
            @Override
            public void interfaceResponse(int position) {
                // open detailed view
                Intent intent = new Intent(context, VoucherActivity.class);
                intent.putExtra(Constants.EXTRA_VOUCHER_TITLE, ((ArticleLocal) dataUi.get(position)).getTitle());
                intent.putExtra(Constants.EXTRA_VOUCHER_HTML_CONTENT, ((ArticleLocal) dataUi.get(position)).getContent());
                intent.putExtra(Constants.EXTRA_VOUCHER_IMAGE_URL, ((ArticleLocal) dataUi.get(position)).getImageUrl());
                intent.putExtra(Constants.EXTRA_VOUCHER_ID, ((ArticleLocal) dataUi.get(position)).getId());
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
                recyclerEmptyText.setVisibility(View.GONE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }
}