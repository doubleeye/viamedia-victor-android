package com.viamedia.victor.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.fragments.HtmlRawFragment;

public class VoucherActivity extends AppCompatActivity {
    private static final String TAG = "VoucherActivity";
    private String id, title, htmlContent, imageUrl;
    private Context context;
    private ImageView articleImage;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher);
        context = this;

        if (getIntent().hasExtra(Constants.EXTRA_VOUCHER_TITLE) &&
                getIntent().hasExtra(Constants.EXTRA_VOUCHER_HTML_CONTENT) &&
                getIntent().hasExtra(Constants.EXTRA_VOUCHER_IMAGE_URL) &&
                getIntent().hasExtra(Constants.EXTRA_VOUCHER_ID)) {

            title = getIntent().getStringExtra(Constants.EXTRA_VOUCHER_TITLE);
            htmlContent = getIntent().getStringExtra(Constants.EXTRA_VOUCHER_HTML_CONTENT);
            imageUrl = getIntent().getStringExtra(Constants.EXTRA_VOUCHER_IMAGE_URL);
            id = getIntent().getStringExtra(Constants.EXTRA_VOUCHER_ID);

            initManagers();
            initUi();
            loadData();
        } else {
            finish();
        }
    }

    private void initManagers() {
    }

    private void initUi() {
        articleImage = (ImageView) findViewById(R.id.toolbar_image);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadData() {
        // load toolbar image
        Glide
                .with(context)
                .load(imageUrl)
                .crossFade()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new articleImage once a day
                .error(R.drawable.flavoured_category_banner) // fallback articleImage
                .into(articleImage);

        // load html content into fragment
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() == 0 ||
                getSupportFragmentManager().getFragments() == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(
                            R.id.nested_scroll_view,
                            HtmlRawFragment.newInstance(htmlContent), null
                    )
                    .disallowAddToBackStack()
                    .commit();
        }
    }
}