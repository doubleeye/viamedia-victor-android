package com.viamedia.victor.base.api.apiModels;

public class FeaturedImage {
    private String path;
    private String medium_path;
    private String thumbnail_path;
    private String caption;

    public String getThumbnailPath() {
        return thumbnail_path;
    }

    public void setMediumPath(String medium_path) {
        this.medium_path = medium_path;
    }

    public String getMediumPath() {
        return medium_path;
    }

    public void setThumbnailPath(String thumbnail_path) {
        this.thumbnail_path = thumbnail_path;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
