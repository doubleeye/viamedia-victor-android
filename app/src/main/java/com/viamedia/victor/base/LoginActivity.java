package com.viamedia.victor.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.RequestFacebookLogin;
import com.viamedia.victor.base.api.apiModels.RequestLogin;
import com.viamedia.victor.base.api.apiModels.RequestRegistration;
import com.viamedia.victor.base.api.apiModels.ResponseBaseLoginRegistration;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.customDialogs.CustomDialogFacebook;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.managers.UserManager;
import com.viamedia.victor.base.managers.ValidationManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private CallbackManager callbackManager;
    private ProfileTracker mProfileTracker;
    private EditText txtMsisdn, txtPass;
    private Context context;
    private Button submitButton;
    private UserManager userManager;
    private Util util;
    private ImageUtil imageUtil;
    private WebService webService;
    private StorageManager storageManager;
    private ValidationManager validationManager;
    private User user;
    private Activity activity;
    private int isCompleteasyncTask1, isCompleteasyncTask2 = -1;
    private String facebookId,
            facebookFirstName,
            facebookLastName,
            facebookEmail,
            facebookProfilePictureUri,
            facebookAccessToken,
            facebookGender,
            facebookBirthday,
            facebookLocation,
            formattedNumberWithPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        activity = this;

        initManagers();
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadUserData();
    }

    @Override
    protected void onPause() {
        saveUserData();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // this will trigger the callbacks for the facebook login button
        MyApp.getInstance().getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        imageUtil = MyApp.getInstance().getImageUtil();
        callbackManager = MyApp.getInstance().getCallbackManager();
        userManager = MyApp.getInstance().getUserManager();
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
        validationManager = MyApp.getInstance().getValidationManager();
    }

    public EditText getMsisdnEditText() {
        return txtMsisdn;
    }

    private void initUi() {
        txtMsisdn = (EditText) findViewById(R.id.msisdn);
        txtPass = (EditText) findViewById(R.id.password);

        uiMsisdnTextChangeValidation();
        uiForgotPassword();
        uiCustomSignIn();
        uiFacebookSignIn();
        uiCustomRegistration();
    }

    private void uiCustomRegistration() {
        Button buttonRegister = (Button) findViewById(R.id.register_button);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void uiFacebookSignIn() {
        LoginButton buttonSignInFacebook = (LoginButton) findViewById(R.id.facebook_sign_in_button);
        buttonSignInFacebook.setReadPermissions(Arrays.asList("email", "user_location", "user_about_me", "user_birthday"));
        buttonSignInFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "Facebook login onSuccess()");
                showLoading();

                // Async call #1
                // get facebook profile
                if (Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                            Log.i(TAG, "Facebook - profile: " + profile2.getFirstName());
                            mProfileTracker.stopTracking();

                            // save facebook profile data
                            facebookId = profile2.getId();
                            facebookFirstName = profile2.getFirstName();
                            facebookLastName = profile2.getLastName();
                            facebookProfilePictureUri = String.format(getString(R.string.graph_facebook_profile_picture), facebookId);
                            waitForBothFacebookAsyncTasksAndLoginUser(1, -1);
                        }
                    };
                    mProfileTracker.startTracking();
                } else {
                    Profile profile = Profile.getCurrentProfile();
                    Log.i(TAG, "Facebook - profile: " + profile.getFirstName());

                    // save facebook profile data
                    facebookId = profile.getId();
                    facebookFirstName = profile.getFirstName();
                    facebookLastName = profile.getLastName();
                    facebookProfilePictureUri = String.format(getString(R.string.graph_facebook_profile_picture), facebookId);
                    waitForBothFacebookAsyncTasksAndLoginUser(1, -1);
                }

                // save facebook token to device
                facebookAccessToken = loginResult.getAccessToken().getToken();

                // Async call #2
                // get additional information about the user
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i(TAG, response.toString());

                        // extract extra permission data
                        try {
                            facebookEmail = object.getString("email");
                        } catch (JSONException e) {
                            Log.e(TAG, "Facebook email unavailable");
                        }

                        try {
                            facebookGender = object.getString("gender");
                        } catch (JSONException e) {
                            Log.e(TAG, "Facebook gender not available");
                        }

                        try {
                            facebookBirthday = object.getString("birthday");
                        } catch (JSONException e) {
                            Log.e(TAG, "Facebook gender not available");
                        }
                        waitForBothFacebookAsyncTasksAndLoginUser(-1, 1);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,location,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "Facebook login onCancel()");
            }

            @Override
            public void onError(FacebookException e) {
                Snackbar.make(findViewById(android.R.id.content), R.string.facebook_sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                Log.e(TAG, e.getMessage());
            }
        });
    }

    private void uiCustomSignIn() {
        submitButton = (Button) findViewById(R.id.sign_in_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValidation();
            }
        });
    }

    private void uiForgotPassword() {
        TextView forgotPassword = (TextView) findViewById(R.id.forgot_password);
        SpannableString str = new SpannableString(forgotPassword.getText());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        forgotPassword.setText(str);
        forgotPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    showForgotPasswordActvity();
                    v.performClick();
                }
                return true;
            }
        });
    }

    private void uiMsisdnTextChangeValidation() {
        txtMsisdn.addTextChangedListener(
                new PhoneNumberFormattingTextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        formattedNumberWithPlus = validationManager.isPhoneNumberValid(String.valueOf(s));
                        if (formattedNumberWithPlus != null) {
                            submitButton.setEnabled(true);
                        } else {
                            submitButton.setEnabled(false);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                }
        );
    }

    private void waitForBothFacebookAsyncTasksAndLoginUser(int asyncTask1, int asyncTask2) {
        if (asyncTask1 > -1) {
            isCompleteasyncTask1 = asyncTask1;
            Log.i(TAG, "Sign in facebook user, async #1 complete");
        }
        if (asyncTask2 > -1) {
            isCompleteasyncTask2 = asyncTask2;
            Log.i(TAG, "Sign in facebook user, async #2 complete");
        }

        if (isCompleteasyncTask1 == 1 && isCompleteasyncTask2 == 1) {
            Log.i(TAG, "Sign in facebook user, both async tasks are complete");

            // ok, sign in user
            if (facebookEmail != null && facebookEmail.length() > 0 && facebookId != null && facebookId.length() > 0) {
                // sign user in on our server
                apiFacebookLogin(facebookId, facebookFirstName, facebookLastName, facebookEmail, facebookAccessToken);
            } else {
                Snackbar.make(findViewById(android.R.id.content), R.string.facebook_sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                facebookLogout();
                hideLoading();
            }
        }
    }

    private void saveUserData() {
        if (user == null) {
            // try to load from local device 1st
            user = storageManager.getUserObject();

            // fallback
            if (user == null) {
                user = new User();
            }
        }

        if (facebookId != null) {
            user.setFacebookId(facebookId);
        }
        if (facebookFirstName != null) {
            user.setFirstname(facebookFirstName);
        }
        if (facebookLastName != null) {
            user.setLastname(facebookLastName);
        }
        if (facebookEmail != null) {
            user.setEmail(facebookEmail);
        }
        if (facebookLocation != null) {
            user.setCity(facebookLocation);
        }
        if (facebookGender != null) {
            user.setGender(facebookGender);
        }
        if (facebookBirthday != null) {
            user.setBirthday(facebookBirthday);
        }
        if (facebookAccessToken != null) {
            user.setFacebookLoginToken(facebookAccessToken);
        }
        if (formattedNumberWithPlus != null) {
            user.setMsisdnWithoutPlus(formattedNumberWithPlus);
        }
        storageManager.saveDataToFile(new File(storageManager.getUserFilename()), (new Gson()).toJson(user));
    }

    private void loadUserData() {
        user = storageManager.getUserObject();

        // default/fallback
        txtMsisdn.setText(
                String.format(
                        Locale.getDefault(),
                        getString(R.string.msisdn_with_plus),
                        MyApp.getInstance().getUserManager().getCountryDialCode()));

        if (user != null) {
            if (user.getMsisdnWithoutPlus() != null && user.getMsisdnWithoutPlus().length() > 0) {
                formattedNumberWithPlus = validationManager.isPhoneNumberValid(user.getMsisdnWithoutPlus());
                if (formattedNumberWithPlus == null) {
                    formattedNumberWithPlus = validationManager.isPhoneNumberValid(user.getMsisdnWithPlus());
                }

                if (formattedNumberWithPlus != null) {
                    txtMsisdn.setText(formattedNumberWithPlus);
                }
            }
            if (facebookEmail != null) {
                user.setEmail(facebookEmail);
            }
            if (facebookGender != null) {
                user.setGender(facebookGender);
            }
            if (facebookBirthday != null) {
                user.setBirthday(facebookBirthday);
            }
            if (facebookLocation != null) {
                user.setCity(facebookLocation);
            }
            if (facebookId != null) {
                user.setFacebookId(facebookId);
            }
            if (facebookFirstName != null) {
                user.setFirstname(facebookFirstName);
            }
            if (facebookLastName != null) {
                user.setLastname(facebookFirstName);
            }
        }

        // update cursor position of first field
        txtMsisdn.setSelection(txtMsisdn.getText().length());
    }

    private void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void showMainAcitvity(String extraKey) {
        Intent intent = new Intent(context, MainActivity.class);
        if (extraKey != null) {
            intent.putExtra(extraKey, true);
        }
        startActivity(intent);
        finish();
    }

    private void apiLogin(final String number, final String password) {
        showLoading();

        final RequestLogin requestLogin = new RequestLogin();
        requestLogin.setMsisdn(util.removePlusFromNumber(number));
        requestLogin.setPassword(password);

        Call<ResponseBaseLoginRegistration> call = webService.login(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                requestLogin
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response.body() != null &&
                        response.body().getData() != null &&
                        response.body().getData().getToken() != null) {

                    // token will be used for future calls after login
                    Log.i(TAG, "login token: " + response.body().getData().getToken());

                    if (user == null) {
                        user = new User();
                    }

                    // save user data
                    user.setFirstname(response.body().getData().getFirstName());
                    user.setLastname(response.body().getData().getLastName());
                    user.setLoginToken(response.body().getData().getToken());
                    user.setId(response.body().getData().getId());
                    user.setEmail(response.body().getData().getEmail());
                    user.setAvatar(response.body().getData().getAvatar());

                    user.setCity(response.body().getData().getCity());
                    user.setCountry(response.body().getData().getCountry());
                    user.setGender(response.body().getData().getGender().equalsIgnoreCase("m") ? Constants.MALE : Constants.FEMALE);
                    user.setAge(response.body().getData().getAge());

                    // show main activity
                    showMainAcitvity(Constants.EXTRA_LOGIN_SUCCESS);
                } else if (response != null &&
                        response.body() != null &&
                        response.body().getMessage().equalsIgnoreCase(Constants.RESPONSE_ERROR_MESSAGE_AUTHENTICATION_FAILED)) {

                    Snackbar.make(findViewById(android.R.id.content), R.string.Incorrect_login_details_please_try_again, Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
            }
        });
    }

    private void apiFacebookLogin(
            final String facebookId,
            final String facebookFirstName,
            final String facebookLastName,
            final String facebookEmail,
            final String facebookToken) {

        // create request
        final RequestFacebookLogin requestFacebookLogin = new RequestFacebookLogin();
        requestFacebookLogin.setEmail(facebookEmail);
        requestFacebookLogin.setFacebookToken(facebookToken);
        requestFacebookLogin.setFacebookUserId(facebookId);

        Call<ResponseBaseLoginRegistration> call = webService.facebookLogin(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                requestFacebookLogin
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");

                if (response.body() != null && response.body().getData() != null && response.body().getData().getToken() != null) {
                    // token will be used for future calls after login
                    Log.i(TAG, "login token: " + response.body().getData().getToken());

                    // save user data
                    user.setFirstname(response.body().getData().getFirstName());
                    user.setLastname(response.body().getData().getLastName());
                    user.setLoginToken(response.body().getData().getToken());
                    user.setId(response.body().getData().getId());
                    user.setEmail(response.body().getData().getEmail());
                    user.setAvatar(response.body().getData().getAvatar());

                    user.setCity(response.body().getData().getCity());
                    user.setCountry(response.body().getData().getCountry());
                    user.setGender(response.body().getData().getGender().equalsIgnoreCase("m") ? Constants.MALE : Constants.FEMALE);
                    user.setAge(response.body().getData().getAge());

                    // show main activity
                    showMainAcitvity(Constants.EXTRA_LOGIN_SUCCESS);
                } else if (response.code() == 404) {
                    // required fields for registration, facebook
                    final String facebookAge = util.getAgeFromFacebookBirthday(facebookBirthday);
                    final String facebookGenderShort = util.getGenderFromFacebookGender(facebookGender);
                    // always ask for city and country

                    // facebook user was not found on our server
                    // ask user for phone number, if user entered valid number register a new user via our api
                    hideLoading();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new CustomDialogFacebook(
                                    activity,
                                    facebookId,
                                    facebookFirstName,
                                    facebookLastName,
                                    facebookEmail,
                                    facebookToken,
                                    facebookAge,
                                    facebookGenderShort,
                                    user,
                                    facebookProfilePictureUri
                            ).show();
                        }
                    });
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                    facebookLogout();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                    facebookLogout();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                hideLoading();
                facebookLogout();
            }
        });
    }

    public void facebookLogout() {
        // clear token
        if (user != null) {
            user.setFacebookLoginToken(null);
        }
        // log out facebook user
        LoginManager.getInstance().logOut();
    }

    public void apiFacebookRegister(
            final String msisdn,
            final String facebookUserId,
            final String firstname,
            final String lastname,
            final String email,
            final String facebookToken,
            final String gender,
            final String age,
            final String city,
            final String country,
            final String avatar) {

        showLoading();

        final RequestRegistration requestRegistration = new RequestRegistration();
        requestRegistration.setMsisdn(msisdn);
        requestRegistration.setFirstName(firstname);
        requestRegistration.setLastName(lastname);
        requestRegistration.setEmail(email);
        requestRegistration.setFacebookUserId(facebookUserId);
        requestRegistration.setFacebookToken(facebookToken);
        requestRegistration.setGender(gender);
        requestRegistration.setAge(age);
        requestRegistration.setCity(city);
        requestRegistration.setCountry(country);
        requestRegistration.setAvatar(avatar); // avatar received only for facebook registration
        requestRegistration.setDeviceCode(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.GCM_CLIENT_TOKEN, null));

        Call<ResponseBaseLoginRegistration> call = webService.register(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                requestRegistration
        );
        call.enqueue(new Callback<ResponseBaseLoginRegistration>() {
            @Override
            public void onResponse(Call<ResponseBaseLoginRegistration> call, Response<ResponseBaseLoginRegistration> response) {
                Log.i(TAG, "onSuccess()");
                hideLoading();

                if (response.body() != null && response.body().getData() != null && response.body().getData().getToken() != null) {
                    // on success registration, token will be received, user will be logged in automatically on server side
                    // token will be used for future calls after login
                    Log.i(TAG, "login token: " + response.body().getData().getToken());

                    // save user data
                    user.setFirstname(response.body().getData().getFirstName());
                    user.setLastname(response.body().getData().getLastName());
                    user.setLoginToken(response.body().getData().getToken());
                    user.setId(response.body().getData().getId());
                    user.setFacebookId(response.body().getData().getFacebookUserId());
                    user.setEmail(response.body().getData().getEmail());
                    user.setAvatar(response.body().getData().getAvatar());

                    user.setCity(response.body().getData().getCity());
                    user.setCountry(response.body().getData().getCountry());
                    user.setAge(response.body().getData().getAge());
                    user.setGender(response.body().getData().getGender());

                    // show main activity
                    // show login success, registration happens in background
                    showMainAcitvity(Constants.EXTRA_LOGIN_SUCCESS);
                } else {
                    // inform user that login has failed, registration happens in the background for facebook
                    Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                    facebookLogout();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseLoginRegistration> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                hideLoading();

                // inform user that login has failed, registration happens in the background for facebook
                Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_failed_please_try_again, Snackbar.LENGTH_LONG).show();
                facebookLogout();
            }
        });
    }

    private void showForgotPasswordActvity() {
        startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
    }

    public void submitValidation() {
        if (validationManager.isLoginFormValid(
                findViewById(android.R.id.content),
                txtPass)) {

            apiLogin(formattedNumberWithPlus, String.valueOf(txtPass.getText()));
        }
    }
}