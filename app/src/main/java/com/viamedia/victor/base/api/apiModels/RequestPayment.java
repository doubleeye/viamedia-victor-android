package com.viamedia.victor.base.api.apiModels;

public class RequestPayment {
    private int payment_option_id;
    private PaymentObject payment_object;

    public class PaymentObject {
        private int id;
        private String type;

        public PaymentObject(int id, String type) {
            this.id = id;
            this.type = type;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public int getPaymentOptionId() {
        return payment_option_id;
    }

    public void setPaymentObject(int id, String type) {
        this.payment_object = new PaymentObject(id, type);
    }

    public void setPaymentObject(PaymentObject payment_object) {
        this.payment_object = payment_object;
    }

    public PaymentObject getPaymentObject() {
        return payment_object;
    }

    public void setPaymentOptionId(int payment_option_id) {
        this.payment_option_id = payment_option_id;
    }
}
