package com.viamedia.victor.base.adapters;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.viamedia.victor.base.R;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.models.InterestsLocal;

public class MyInterestsAdapter extends RecyclerView.Adapter<MyInterestsAdapter.ViewHolder> {
    private static final String TAG = "MyInterestsAdapter";
    private InterestsLocal data;
    private Context context;
    private AdapterInterface adapterInterface;

    public MyInterestsAdapter(Context context, InterestsLocal data, AdapterInterface adapterInterface) {
        this.data = data;
        this.context = context;
        this.adapterInterface = adapterInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public FrameLayout toggleLayout;
        public ImageView image;
        public TextView title;
        private AdapterInterface adapterInterface;

        public ViewHolder(final View itemView, final AdapterInterface adapterInterface) {
            super(itemView);
            mView = itemView;

            // ui
            toggleLayout = (FrameLayout) itemView.findViewById(R.id.toggle_layout);
            image = (ImageView) itemView.findViewById(R.id.adapter_item_image);
            title = (TextView) itemView.findViewById(R.id.item_title);

            toggleLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // invert image color and set data
                    ColorMatrix matrix = new ColorMatrix();
                    if (v.getTag().equals(Constants.EXTRA_INTERESTS_CHECKED)) {
                        matrix.setSaturation(0);
                        v.setTag(Constants.EXTRA_INTERESTS_UNCHECKED);
                    } else {
                        matrix.setSaturation(1);
                        v.setTag(Constants.EXTRA_INTERESTS_CHECKED);
                    }
                    ((ImageView) v.findViewById(R.id.adapter_item_image)).setColorFilter(new ColorMatrixColorFilter(matrix));

                    adapterInterface.interfaceResponse(
                            getAdapterPosition(),
                            v.getTag().equals(Constants.EXTRA_INTERESTS_CHECKED)
                    );
                }
            });
        }
    }

    @Override
    public MyInterestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.my_interests_list_item, parent, false);
        return new ViewHolder(view, adapterInterface);
    }

    @Override
    public void onBindViewHolder(final MyInterestsAdapter.ViewHolder holder, int position) {
        // default view
        holder.toggleLayout.setTag(data.getInterestsLocal().get(position).isEnabled() ? Constants.EXTRA_INTERESTS_CHECKED : Constants.EXTRA_INTERESTS_UNCHECKED);
        holder.title.setText(data.getInterestsLocal().get(position).getTitle());

        // get resource id
        int resId = -1;
        try {
            resId = Integer.parseInt(data.getInterestsLocal().get(position).getImage());
        } catch (NumberFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // show drawable
        if (resId > -1) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, resId));
        } else {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.flavoured_article_image));
        }

        // image color
        ColorMatrix matrix = new ColorMatrix();
        if (data.getInterestsLocal().get(position).isEnabled()) {
            matrix.setSaturation(1);
        } else {
            matrix.setSaturation(0);
        }
        holder.image.setColorFilter(new ColorMatrixColorFilter(matrix));
    }

    @Override
    public int getItemCount() {
        return data.getInterestsLocal().size();
    }

    public interface AdapterInterface {
        void interfaceResponse(int position, boolean isChecked);
    }
}