package com.viamedia.victor.base.api.apiModels;

import com.viamedia.victor.base.constants.Constants;

import java.util.ArrayList;

public class ResponseBaseStoreFront extends ResponseBase {
    private DataObject data;

    public DataObject getData() {
        return data;
    }

    public void setData(DataObject data) {
        this.data = data;
    }

    public class DataObject {
        private String id;
        private String name;
        private String slug;
        private String description;
        private String url;
        private ArrayList<ResponseStoreFrontSection> sections;
        private ArrayList<ResponseStoreFrontVoucher> vouchers;

        // sections
        public class ResponseStoreFrontSection {
            private int id;
            private String name;
            private String url;
            private ArrayList<ResponseStoreFrontSectionCategory> categories;
            private ResponseStoreFrontSectionPricingStrategy pricing_strategy;
            private ArrayList<ResponseStoreFrontSectionPaymentOption> payment_options;

            // categories
            public class ResponseStoreFrontSectionCategory {
                private int id;
                private String name;
                private String url;
                private ResponseStoreFrontSectionCategoryPricingStrategy pricing_strategy;

                public class ResponseStoreFrontSectionCategoryPricingStrategy {
                    private String status;
                    private double price;

                    public String getStatus() {
                        return status;
                    }

                    public void setStatus(String status) {
                        this.status = status;
                    }

                    public double getPrice() {
                        return price;
                    }

                    public void setPrice(double price) {
                        this.price = price;
                    }
                }

                public ResponseStoreFrontSectionCategoryPricingStrategy getPricingStrategy() {
                    return pricing_strategy;
                }

                public void setPricingStrategy(ResponseStoreFrontSectionCategoryPricingStrategy pricing_strategy) {
                    this.pricing_strategy = pricing_strategy;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public class ResponseStoreFrontSectionPricingStrategy {
                private String status;
                private double price;

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public double getPrice() {
                    return price;
                }

                public void setPrice(double price) {
                    this.price = price;
                }
            }

            public class ResponseStoreFrontSectionPaymentOption {
                private int option_id;
                private String option_name;
                private String option_type;

                public int getOptionId() {
                    return option_id;
                }

                public void setOptionId(int option_id) {
                    this.option_id = option_id;
                }

                public String getOptionName() {
                    return option_name;
                }

                public void setOptionName(String option_name) {
                    this.option_name = option_name;
                }

                public String getOptionType() {
                    return option_type;
                }

                public void setOptionType(String option_type) {
                    this.option_type = option_type;
                }
            }

            public ArrayList<ResponseStoreFrontSectionPaymentOption> getPaymentOptions() {
                return payment_options;
            }

            public void setPaymentOptions(ArrayList<ResponseStoreFrontSectionPaymentOption> payment_options) {
                this.payment_options = payment_options;
            }

            public ResponseStoreFrontSectionPricingStrategy getPricingStrategy() {
                return pricing_strategy;
            }

            public void setPricingStrategy(ResponseStoreFrontSectionPricingStrategy pricing_strategy) {
                this.pricing_strategy = pricing_strategy;
            }

            public ArrayList<ResponseStoreFrontSectionCategory> getCategories() {
                return categories;
            }

            public void setCategories(ArrayList<ResponseStoreFrontSectionCategory> categories) {
                this.categories = categories;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        // vouchers
        public class ResponseStoreFrontVoucher {
            private String title;
            private String image;
            private String uuid;
            private String valid_from;
            private String valid_to;
            private String content;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getValidFrom() {
                return valid_from;
            }

            public void setValidFrom(String valid_from) {
                this.valid_from = valid_from;
            }

            public String getValidTo() {
                return valid_to;
            }

            public void setValidTo(String valid_to) {
                this.valid_to = valid_to;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public ArrayList<ResponseStoreFrontVoucher> getVouchers() {
            return vouchers;
        }

        public void setVouchers(ArrayList<ResponseStoreFrontVoucher> vouchers) {
            this.vouchers = vouchers;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

//        public ResponseStoreFrontParent getParent() {
//            return parent;
//        }

//        public void setParent(ResponseStoreFrontParent parent) {
//            this.parent = parent;
//        }

        public ArrayList<ResponseStoreFrontSection> getSectionsWithoutHome() {
            ArrayList<ResponseStoreFrontSection> sectionsWithoutHome = new ArrayList<>();

            for (ResponseStoreFrontSection section : sections) {
                if (section.getName() != null && !section.getName().equals(Constants.SECTION_HOME_FEED)) {
                    sectionsWithoutHome.add(section);
                }
            }
            return sectionsWithoutHome;
        }

        public ResponseStoreFrontSection getSectionHome() {
            for (ResponseStoreFrontSection section : sections) {
                if (section.getName() != null && section.getName().equals(Constants.SECTION_HOME_FEED)) {
                    return section;
                }
            }
            return null;
        }

        public void setSections(ArrayList<ResponseStoreFrontSection> sections) {
            this.sections = sections;
        }
    }
}