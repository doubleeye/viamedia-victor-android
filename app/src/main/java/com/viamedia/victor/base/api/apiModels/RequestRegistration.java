package com.viamedia.victor.base.api.apiModels;

public class RequestRegistration {
    private String msisdn;
    private String password;
    private String password_confirmation;
    private String first_name;
    private String last_name;
    private String email;
    private String city;
    private String country;
    private String facebook_user_id;
    private String facebook_token;
    private String age;
    private String gender;
    private String avatar;
    private String device_code;

    public String getDeviceCode() {
        return device_code;
    }

    public void setDeviceCode(String device_code) {
        this.device_code = device_code;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAge() {
        return age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFacebookToken() {
        return facebook_token;
    }

    public void setFacebookToken(String facebook_token) {
        this.facebook_token = facebook_token;
    }

    public void setFacebookUserId(String facebook_user_id) {
        this.facebook_user_id = facebook_user_id;
    }

    public String getFacebookUserId() {
        return facebook_user_id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return password_confirmation;
    }

    public void setPasswordConfirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }
}