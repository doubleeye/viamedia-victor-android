package com.viamedia.victor.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.viamedia.victor.base.adapters.ArticleListAdapter;
import com.viamedia.victor.base.api.WebService;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.api.apiModels.ResponseBaseStoreFront;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.PermissionManager;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.User;
import com.viamedia.victor.base.services.GcmRegistrationIntentService;
import com.viamedia.victor.base.util.ImageUtil;
import com.viamedia.victor.base.util.Util;

import java.io.File;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivityArticleList {
    private static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private TextView recyclerEmptyText;
    private Context context;
    private Toolbar toolbar;
    private Activity activity;
    private NavigationView navigationView;
    private LinearLayout navWrap;
    private TextView navTitle;
    private TextView navDescription;
    private ImageView navImage;
    private CircleImageView navProfileImage;
    private String searchString;
    private SearchView searchView;
    private Util util;
    private ImageUtil imageUtil;
    private WebService webService;
    private StorageManager storageManager;
    private PermissionManager permissionManager;
    private ArticleListAdapter adapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        initManagers();
        initUi();
        readExtras();
        setupNavDrawer();

        // hand off longer operations to thread
        new Thread() {
            @Override
            public void run() {
                // data
                loadDataLocal();
                loadDataRemote();

                // register device to receive push notifications
                initGcmRegistrationReceiver();
                registerGcmRegistrationReceiver();
                if (checkPlayServices()) {
                    Intent intent = new Intent(context, GcmRegistrationIntentService.class);
                    startService(intent);
                }

                // check location
                // If the user accepts one permission in a group/category they accept the entire group
                if (permissionManager.hasPermission(context, Constants.PERMISSION_ACCESS_COARSE_LOCATION)) {
                    // continue with normal operation
                    util.locationCheck(activity);
                } else {
                    if (permissionManager.shouldWeAsk(context, Constants.PERMISSION_ACCESS_COARSE_LOCATION)) {
                        String[] perms = {Constants.PERMISSION_ACCESS_COARSE_LOCATION};
                        permissionManager.askPermissions(activity, 200, perms);
                    } else {
                        // inform user that permission was denied
                        Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_denied_by_user, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApp.getInstance().startGpsService();
        loadProfileImage();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                boolean perm1 = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (perm1) {
                    // If the user accepts one permission in a group/category they accept the entire group
                    util.locationCheck(activity);
                    Snackbar.make(findViewById(android.R.id.content), R.string.permission_was_successfully_granted, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        // set icon drawable
        // my profile(custom)
        if (storageManager.getUserObject() != null &&
                storageManager.getUserObject().getLoginToken() != null &&
                storageManager.getUserObject().getFacebookId() == null) {
            menu.findItem(R.id.action_login_status).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_lock_closed));
        }
        // my profile(facebook)
        else if (storageManager.getUserObject() != null && storageManager.getUserObject().getFacebookId() != null) {
            menu.findItem(R.id.action_login_status).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_lock_closed));
        }
        // not logged in
        // login/registration
        else {
            menu.findItem(R.id.action_login_status).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_lock_open));
        }
        menu.findItem(R.id.action_search).setIcon(ContextCompat.getDrawable(context, R.drawable.icon_search));

        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        if (null != searchView) {
            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
                public boolean onQueryTextChange(String newText) {
                    return true;
                }

                public boolean onQueryTextSubmit(String query) {
                    Log.e(TAG, query);

                    searchString = query;
                    searchView.setQuery("", false);
                    searchView.setIconified(true);

                    // show search activity
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    intent.putExtra(Constants.EXTRA_SEARCH_STRING, searchString);
                    startActivity(intent);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        return true;
    }

    private void readExtras() {
        if (getIntent().getExtras() != null && getIntent().getExtras().size() > 0) {
            if (getIntent().getExtras().getBoolean(Constants.EXTRA_REGISTRATION_SUCCESS)) {
                Snackbar.make(findViewById(android.R.id.content), R.string.registration_was_successful, Snackbar.LENGTH_LONG).show();
            } else if (getIntent().getExtras().getBoolean(Constants.EXTRA_LOGIN_SUCCESS)) {
                Snackbar.make(findViewById(android.R.id.content), R.string.sign_in_was_successful, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void initManagers() {
        util = MyApp.getInstance().getUtil();
        imageUtil = MyApp.getInstance().getImageUtil();
        webService = MyApp.getInstance().getWebService();
        storageManager = MyApp.getInstance().getStorageManager();
        permissionManager = MyApp.getInstance().getPermissionManager();
    }

    private void initUi() {
        activity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerEmptyText = (TextView) findViewById(R.id.recycler_view_empty_text_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setupNavDrawer() {
        // drawer toggle
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // drawer item click
//        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navWrap = (LinearLayout) navigationView.getHeaderView(0).findViewById(R.id.nav_header_wrap);
        navTitle = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_title);
        navDescription = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_description);
        navImage = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.nav_header_image);
        navProfileImage = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.nav_profile_image);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // dynamic drawer items onclick
                startActivity(item.getIntent());

                // close drawer
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void loadDataLocal() {
        showLoading();

        // pre load offline data if any
        addStaticMenuItems();
        initAdapterWithData(storageManager.getContentSelectionObject(Constants.SECTION_STATIC_HOME));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (storageManager.getUserObject() != null) {
                    // set nav details
                    if (storageManager.getUserObject().getFirstname() != null &&
                            storageManager.getUserObject().getLastname() != null) {

                        navTitle.setText(
                                String.format(
                                        Locale.getDefault(),
                                        getString(R.string.first_name_space_last_name),
                                        storageManager.getUserObject().getFirstname(),
                                        storageManager.getUserObject().getLastname()));
                    } else {
                        navTitle.setText(getString(R.string.app_name));
                    }
                    if (storageManager.getUserObject().getEmail() != null && !storageManager.getUserObject().getEmail().equalsIgnoreCase("")) {
                        navDescription.setText(storageManager.getUserObject().getEmail());
                        navDescription.setVisibility(View.VISIBLE);
                    } else {
                        navDescription.setVisibility(View.GONE);
                    }

                    // load profile image
                    loadProfileImage();
                }
            }
        });
    }

    private void loadProfileImage() {
        if (storageManager.getUserObject() != null) {
            // custom profile image
            if (storageManager.getUserObject().getAvatarBitmap(imageUtil) != null) {
                // load profile image from device
                navProfileImage.setVisibility(View.VISIBLE);
                navProfileImage.setImageBitmap(storageManager.getUserObject().getAvatarBitmap(imageUtil));
            } else if (storageManager.getUserObject().getAvatar() != null) {
                // load profile image from url
                navProfileImage.setVisibility(View.VISIBLE);
                Glide
                        .with(context)
                        .load(storageManager.getUserObject().getAvatar())
                        .crossFade()
                        .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (R.integer.image_expiry_time_ms_one_day_24x60x60x1000)))) // fetch new image once a day
                        .into(navProfileImage);

            }
        } else {
            navProfileImage.setVisibility(View.GONE);
        }
    }

    private void loadDataRemote() {
        apiStoreFront();
    }

    private void initAdapterWithData(final ResponseBaseContentSelectionOrSection result) {
        if (result != null && result.getData() != null && recyclerView != null) {
            // new adapter
            adapter = new ArticleListAdapter(activity, result, null, 1, new ArticleListAdapter.AdapterInterface() {
                @Override
                public void interfaceResponse(int position) {
                    handleIntefaceResponse(
                            position,
                            storageManager,
                            util,
                            webService,
                            activity,
                            adapter);
                }
            });
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recyclerView.setAdapter(adapter);
                }
            });
        }
    }

    private MenuItem addUniqueMenuItem(final Menu menu, final int var1, final int var2, final int var3, final CharSequence var4, final Intent menuItemIntent) {
        boolean isUnique = true;
        for (int i = 0; i < menu.size(); i++) {
            if (String.valueOf(menu.getItem(i).getTitle()).equalsIgnoreCase(String.valueOf(var4))) {
                isUnique = false;
                break;
            }
        }

        if (isUnique) {
            MenuItem menuItem = menu.add(var1, var2, var3, var4);
            if (menuItemIntent != null) {
                menuItem.setIntent(menuItemIntent);
            }
            return menuItem;
        }
        return null;
    }

    private void addStaticMenuItems() {
        // dynamic menu items will be loaded at the top
        final Menu navigationViewMenu = navigationView.getMenu();

        // vouchers, logged in or not
        if (storageManager != null &&
                storageManager.getStoreFrontObject() != null &&
                storageManager.getStoreFrontObject().getData() != null &&
                storageManager.getStoreFrontObject().getData().getVouchers() != null &&
                storageManager.getStoreFrontObject().getData().getVouchers().size() > 0) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // vouchers
                    addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.vouchers),
                            new Intent(MainActivity.this, VouchersActivity.class));
                }
            });
        }
        // my profile(custom)
        if (storageManager.getUserObject() != null &&
                storageManager.getUserObject().getLoginToken() != null &&
                storageManager.getUserObject().getFacebookId() == null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // custom profile
                    addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.my_profile),
                            new Intent(MainActivity.this, MyProfileCustomActivity.class));

                    navWrap.setVisibility(View.VISIBLE);

                    // sign out
                    MenuItem signOutMenuItem = addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.sign_out),
                            null);

                    if (signOutMenuItem != null) {
                        signOutMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                // clear user data but persist msisdn
                                User userObjectOld = storageManager.getUserObject();
                                String msisdn = userObjectOld.getMsisdnWithPlus();

                                User userObjectNew = new User();
                                userObjectNew.setMsisdnWithoutPlus(msisdn);

                                storageManager.saveDataToFile(
                                        new File(storageManager.getUserFilename()),
                                        (new Gson()).toJson(userObjectNew, User.class)
                                );

                                // clear user interests data
                                new File(storageManager.getInterestsFilename()).delete();

                                // clear glide image cache
                                new Thread(new Runnable() {
                                    public void run() {
                                        Glide.get(context).clearDiskCache();
                                        Glide.get(context).clearMemory();
                                    }
                                });

                                // reload main with logged out user
                                startActivity(new Intent(MainActivity.this, MainActivity.class));
                                finish();

                                return true;
                            }
                        });
                    }
                }
            });
        }
        // my profile(facebook)
        else if (storageManager.getUserObject() != null && storageManager.getUserObject().getFacebookId() != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // facebook profile
                    addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.my_profile),
                            new Intent(MainActivity.this, MyProfileFacebookActivity.class));

                    navWrap.setVisibility(View.VISIBLE);

                    // sign out
                    MenuItem signOutMenuItem = addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.sign_out),
                            null);

                    if (signOutMenuItem != null) {
                        signOutMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                signOut();
                                return true;
                            }
                        });
                    }
                }
            });
        }
        // not logged in
        // login/registration
        else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MenuItem loginRegistrationMenuItem = addUniqueMenuItem(
                            navigationViewMenu,
                            1,
                            Menu.NONE,
                            0,
                            getString(R.string.sign_in_sign_up),
                            null);

                    navWrap.setVisibility(View.GONE);

                    if (loginRegistrationMenuItem != null) {
                        loginRegistrationMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                // show login screen
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                finish();
                                return true;
                            }
                        });
                    }
                }
            });
        }
    }

    private void signOut() {
        // clear user data but persist msisdn
        User userObjectOld = storageManager.getUserObject();
        String msisdn = userObjectOld.getMsisdnWithPlus();

        User userObjectNew = new User();
        userObjectNew.setMsisdnWithoutPlus(msisdn);

        storageManager.saveDataToFile(
                new File(storageManager.getUserFilename()),
                (new Gson()).toJson(userObjectNew, User.class)
        );

        // clear user interests data
        new File(storageManager.getInterestsFilename()).delete();

        // log out facebook user
        LoginManager.getInstance().logOut();

        // clear glide image cache
        new Thread(new Runnable() {
            public void run() {
                Glide.get(context).clearDiskCache();
                Glide.get(context).clearMemory();
            }
        });

        // reload main with logged out user
        startActivity(new Intent(MainActivity.this, MainActivity.class));
        finish();
    }

    private void apiHomeFeed() {
        Call<ResponseBaseContentSelectionOrSection> call = webService.getHomeFeed(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                String.format(getString(R.string.items_range), 0, getResources().getInteger(R.integer.max_result_home_feed)),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string)
        );
        call.enqueue(new Callback<ResponseBaseContentSelectionOrSection>() {
            @Override
            public void onResponse(Call<ResponseBaseContentSelectionOrSection> call, final Response<ResponseBaseContentSelectionOrSection> response) {
                Log.i(TAG, "onSuccess()");
                if (response.body() != null &&
                        response.body().isSuccess() &&
                        response.body().getData() != null &&
                        response.body().getData().getResults() != null &&
                        response.body().getData().getResults().getContentItems() != null) {

                    // load data
                    initAdapterWithData(response.body());

                    // save for offline use
                    storageManager.saveDataToFile(
                            new File(storageManager.getContentSelectionFilename(Constants.SECTION_STATIC_HOME)),
                            (new Gson()).toJson(response.body()));
                } else {
                    Log.e(TAG, "Failed to retrieve data");
                    Snackbar.make(
                            findViewById(android.R.id.content),
                            R.string.please_check_your_internet_connection,
                            Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
                    recyclerEmptyText.setVisibility(View.VISIBLE);
                }
                hideLoading();
            }

            @Override
            public void onFailure(Call<ResponseBaseContentSelectionOrSection> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                Snackbar.make(
                        findViewById(android.R.id.content),
                        R.string.please_check_your_internet_connection,
                        Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).show();
                recyclerEmptyText.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    private void apiStoreFront() {
        if (adapter != null && adapter.getItemCount() > 0) {
            // dont show loading, already preloaded data
        } else {
            showLoading();
        }
        adapter = null;

        Call<ResponseBaseStoreFront> call = webService.getStoreFrontList(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.channel_id),
                getResources().getString(R.string.app_id),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                getResources().getString(R.string.api_version_string),
                getResources().getString(R.string.store_front)
        );
        call.enqueue(new Callback<ResponseBaseStoreFront>() {
            @Override
            public void onResponse(Call<ResponseBaseStoreFront> call, final Response<ResponseBaseStoreFront> response) {
                Log.i(TAG, "onSuccess()");
                if (response.body() != null && response.body().getData() != null && response.body().isSuccess()) {
                    // save for offline use
                    storageManager.saveDataToFile(
                            new File(storageManager.getStorefrontFilename()),
                            (new Gson()).toJson(response.body())
                    );
                } else {
                    Log.e(TAG, getString(R.string.failed_to_retrieve_data));
                }

                // load menu items
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addDynamicMenuItems(storageManager.getStoreFrontObject());
                        addStaticMenuItems();
                    }
                });
                // load home feed
                apiHomeFeed();
            }

            @Override
            public void onFailure(Call<ResponseBaseStoreFront> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");

                // load menu items
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addDynamicMenuItems(storageManager.getStoreFrontObject());
                        addStaticMenuItems();
                    }
                });
                // load home feed
                apiHomeFeed();
            }
        });
    }

    private void addDynamicMenuItems(final ResponseBaseStoreFront responseBody) {
        // setup dynamic items on drawer
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Menu navigationViewMenu = navigationView.getMenu();
                navigationViewMenu.clear();

                if (responseBody != null && responseBody.getData() != null) {
                    for (int i = 0; i < responseBody.getData().getSectionsWithoutHome().size(); i++) {
                        ResponseBaseStoreFront.DataObject.ResponseStoreFrontSection section = responseBody.getData().getSectionsWithoutHome().get(i);

                        // intent for onclick
                        Intent itemIntent = new Intent(MainActivity.this, TabbedCategoryActivity.class);
                        itemIntent.putExtra(Constants.EXTRA_TABBED_CATEGORY_TITLE, section.getName());
                        itemIntent.putExtra(Constants.EXTRA_TABBED_CATEGORY_SECTION_ID, i);

                        addUniqueMenuItem(
                                navigationViewMenu,
                                0,
                                Menu.NONE,
                                0,
                                section.getName(),
                                itemIntent);
                    }
                }
            }
        });
    }

    // START gcm
    // TODO: is this still required, please check
    private void initGcmRegistrationReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "initGcmRegistrationReceiver onReceive()");

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(Constants.GCM_SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
    }

    private void registerGcmRegistrationReceiver() {
        Log.i(TAG, "registerGcmRegistrationReceiver()");
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.GCM_REGISTRATION_COMPLETE)
            );
            isReceiverRegistered = true;
        }
    }

    private boolean checkPlayServices() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            Log.i(TAG, "checkPlayServices() error");

            if (apiAvailability.isUserResolvableError(resultCode)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        apiAvailability.getErrorDialog(
                                activity,
                                resultCode,
                                Constants.GCM_PLAY_SERVICES_RESOLUTION_REQUEST).show();
                    }
                });
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }

        Log.i(TAG, "checkPlayServices() OK");
        return true;
    }
    // END gcm
}