package com.viamedia.victor.base.customDialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.viamedia.victor.base.BaseActivityArticleList;
import com.viamedia.victor.base.MyApp;
import com.viamedia.victor.base.R;
import com.viamedia.victor.base.adapters.ArticleListAdapter;
import com.viamedia.victor.base.api.apiModels.PaymentOption;
import com.viamedia.victor.base.api.apiModels.PricingStrategy;
import com.viamedia.victor.base.api.apiModels.RequestPayment;
import com.viamedia.victor.base.api.apiModels.ResponseBase;
import com.viamedia.victor.base.api.apiModels.ResponseBaseContentSelectionOrSection;
import com.viamedia.victor.base.constants.Constants;
import com.viamedia.victor.base.managers.StorageManager;
import com.viamedia.victor.base.models.ArticleLocal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogPaymentSelection extends Dialog {
    private static final String TAG = "PaymentSelection";
    private BaseActivityArticleList activity;
    private Dialog dialog;
    private PricingStrategy pricingStrategy;
    private Button positiveButton, cancelButton;
    private ArrayList<PaymentOption> paymentOptions;
    private PaymentOption selectedPaymentOption = null;
    private StorageManager storageManager;
    private TextView title, description;
    private String contentSelection;
    private ArticleListAdapter adapter;

    public CustomDialogPaymentSelection(
            BaseActivityArticleList activity,
            PricingStrategy pricingStrategy,
            ArrayList<PaymentOption> paymentOptions,
            StorageManager storageManager,
            String contentSelection,
            ArticleListAdapter adapter) {

        super(activity);
        this.activity = activity;
        this.dialog = this;
        this.pricingStrategy = pricingStrategy;
        this.paymentOptions = paymentOptions;
        this.storageManager = storageManager;
        this.contentSelection = contentSelection;
        this.adapter = adapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_paid_content_selection);

        initUi();
    }

    private void initUi() {
        title = (TextView) findViewById(R.id.paid_content_title);
        description = (TextView) findViewById(R.id.paid_content_description);

        positiveButton = (Button) findViewById(R.id.positive_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        List<String> list = new ArrayList<>();

        if (paymentOptions != null) {
            for (PaymentOption paymentOption : paymentOptions) {
                list.add(paymentOption.getOptionName());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(activity, R.layout.custom_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
        }

        // actions
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positiveButtonClick(v);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPaymentOption = paymentOptions.get(position);
                if (selectedPaymentOption != null) {
                    title.setText(selectedPaymentOption.getOptionName());
                    description.setText(selectedPaymentOption.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void positiveButtonClick(View v) {
        if (selectedPaymentOption == null) {
            // no option selected
        } else if (selectedPaymentOption.getOptionType().equalsIgnoreCase(Constants.PAID_CONTENT_BILLLING_CHOICE_AD_HOC) ||
                selectedPaymentOption.getOptionType().equalsIgnoreCase(Constants.PAID_CONTENT_BILLLING_CHOICE_SUBSCRIPTION)) {

            apiMakePayment(
                    selectedPaymentOption.getOptionId(),
                    pricingStrategy.getId(),
                    pricingStrategy.getType(),
                    storageManager.getUserObject().getLoginToken());
        }
    }

    private void apiMakePayment(int paymentOptionId, int paymentObjectId, String paymentObjectType, String loginToken) {
        showLoadingDialog();

        final RequestPayment requestPayment = new RequestPayment();
        requestPayment.setPaymentOptionId(paymentOptionId);
        requestPayment.setPaymentObject(paymentObjectId, paymentObjectType);

        Call<ResponseBase> call = MyApp.getInstance().getWebService().makePaymentBilling(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                activity.getResources().getString(R.string.api_key),
                activity.getResources().getString(R.string.channel_id),
                activity.getResources().getString(R.string.app_id),
                loginToken,
                activity.getResources().getString(R.string.api_version_string),
                requestPayment);

        call.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, final Response<ResponseBase> response) {
                Log.i(TAG, "onSuccess()");

                // show status, close dialog
                if (response.body() != null && response.body().isSuccess()) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                snackbarCenter(Snackbar.make(findViewById(android.R.id.content), R.string.success, Snackbar.LENGTH_LONG)).show();
                                Thread.sleep(1000);
                                dialog.cancel();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    t.start();

                    // get updated content selection to reflect paid data
                    apiContentSelection(contentSelection, dialog);
                } else {
                    snackbarCenter(Snackbar.make(findViewById(android.R.id.content), R.string.payment_has_failed_please_try_again, Snackbar.LENGTH_LONG)).show();
                    hideLoadingDialog();
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                snackbarCenter(Snackbar.make(findViewById(android.R.id.content), R.string.payment_has_failed_please_try_again, Snackbar.LENGTH_LONG)).show();
                hideLoadingDialog();
            }
        });
    }

    private void apiContentSelection(final String contentSelection, final Dialog dialog) {
        Call<ResponseBaseContentSelectionOrSection> call = MyApp.getInstance().getWebService().getContentSelection(
                Constants.RESPONSE_TYPE_APPLICATION_JSON,
                activity.getResources().getString(R.string.api_key),
                activity.getResources().getString(R.string.channel_id),
                activity.getResources().getString(R.string.app_id),
                String.format(activity.getString(R.string.items_range), 0, activity.getResources().getInteger(R.integer.max_result_content_selection)),
                storageManager != null && storageManager.getUserObject() != null ? storageManager.getUserObject().getLoginToken() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLatString() : null,
                storageManager != null && storageManager.getGpsServiceObject() != null ? storageManager.getGpsServiceObject().getLngString() : null,
                activity.getResources().getString(R.string.api_version_string),
                contentSelection);

        call.enqueue(new Callback<ResponseBaseContentSelectionOrSection>() {
            @Override
            public void onResponse(Call<ResponseBaseContentSelectionOrSection> call, final Response<ResponseBaseContentSelectionOrSection> response) {
                Log.i(TAG, "onSuccess()");
                if (response.body() != null &&
                        response.body().isSuccess() &&
                        response.body().getData() != null &&
                        response.body().getData().getResults() != null &&
                        response.body().getData().getResults().getContentItems() != null) {

                    // refresh adapter with new data
                    adapter.clearArticleList();
                    final ArrayList<ArticleLocal> data = new ArrayList<>();
                    if (response.body().getData() != null && response.body().getData() != null) {
                        for (final ResponseBaseContentSelectionOrSection.DataObject.ResponseContentSelectionResult.ResponseContentSelectionResultContentItem item : response.body().getData().getResults().getContentItems()) {
                            data.add(new ArticleLocal(item, response.body().getData().getResults().getSlug()));
                        }
                    }
                    adapter.addArticleList(data);
                    adapter.notifyDataSetChanged();

                    // save for offline use
                    storageManager.saveDataToFile(
                            new File(storageManager.getContentSelectionFilename(contentSelection)),
                            (new Gson()).toJson(response.body()));

                    dialog.cancel();
                } else {
                    Log.e(TAG, "Failed to retrieve data");
                    snackbarCenter(Snackbar.make(findViewById(android.R.id.content), R.string.could_not_update_content_selection, Snackbar.LENGTH_LONG)).show();
                }
                hideLoadingDialog();
            }

            @Override
            public void onFailure(Call<ResponseBaseContentSelectionOrSection> call, Throwable t) {
                Log.e(TAG, t != null && t.getMessage() != null ? t.getMessage() : "onFailure()");
                snackbarCenter(Snackbar.make(findViewById(android.R.id.content), R.string.could_not_update_content_selection, Snackbar.LENGTH_LONG)).show();
                hideLoadingDialog();
            }
        });
    }

    private Snackbar snackbarCenter(Snackbar snackbar) {
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        view.setLayoutParams(params);
        return snackbar;
    }

    private void showLoadingDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideLoadingDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading);
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }
}